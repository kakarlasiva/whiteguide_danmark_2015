package com.facebook.samples.fbconnect;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.samples.contacts.SideBar;
import com.facebook.samples.facebook.AsyncFacebookRunner;
import com.facebook.samples.facebook.AsyncFacebookRunner.RequestListener;
import com.facebook.samples.facebook.Facebook;
import com.facebook.samples.facebook.FacebookError;
import com.facebook.samples.facebook.SessionEvents;
import com.facebook.samples.facebook.SessionEvents.AuthListener;
import com.facebook.samples.facebook.SessionEvents.LogoutListener;
import com.facebook.samples.facebook.SessionStore;
import com.facebook.samples.facebook.Util;
import com.facebook.samples.friendpicker.Common;
import com.facebook.samples.friendpicker.R;
import com.facebook.samples.friendpicker.UrlImageLoader;


public class FBFriendsActivity extends Activity {

	private Facebook mFacebook;
	private FacebookUserInfoBean mFacebookUserInfoBean;
	private FbFriend mFbFriendInfoBean;
	private List<FbFriend> listFbFriendInfoBean;
	private ImageView profilePicture;
	private TextView fbUserName;
	private ListView albumsList;
	private Handler mHandler;
	private ProgressDialog mProgressDialog = null;
	private ProgressDialog mProgDialog = null;
	public static final String RUN_ONCE = "facebook";
	public int totalCount = 0;
	private boolean photosOfMeFlag = false;
	private UrlImageLoader urlImageLoader;
	private boolean mIsAlertDisplayed;
	private FBApplicationInfoBean fbApplicationInfoBean;
	private SideBar indexBar;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.facebook_friend_activity);
		urlImageLoader = new UrlImageLoader(getApplicationContext(), true);
		mIsAlertDisplayed = false;
		profilePicture = (ImageView) findViewById(R.id.imv_albums_profile_picture);
		fbUserName = (TextView) findViewById(R.id.tv_albums_fb_userName);
		albumsList = (ListView) findViewById(R.id.albums_list);
		indexBar = (SideBar) findViewById(R.id.sideBar1);
		mFacebook = new Facebook(Util.APP_ID);
		Bundle bundle = this.getIntent().getExtras();

		initializeDataBundle(bundle);
		mHandler = new Handler();
		SessionStore.restore(mFacebook, this);
		SessionEvents.addAuthListener(new FacebookAuthListener());
		SessionEvents.addLogoutListener(new FacebookLogoutListener());
		new LoadApplicationInformation().execute(null, null, null);
		initializeDataBundle(bundle);
		findViewById(R.id.fb_logout).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (!Common.isInternetAvailable(FBFriendsActivity.this)
						|| Common.isAirplaneModeOn(FBFriendsActivity.this)) {
					Common.internetAlertMsg(FBFriendsActivity.this);
					mIsAlertDisplayed = true;
					return;
				}
				mFacebook = new Facebook("193498163996120");
				SessionStore.restore(mFacebook, FBFriendsActivity.this);
				if (mFacebook.isSessionValid()) {
					mProgressDialog = ProgressDialog.show(
							FBFriendsActivity.this,
							"","**************&^", false, false);
					mProgressDialog
							.setOnKeyListener(new DialogInterface.OnKeyListener() {
								@Override
								public boolean onKey(DialogInterface dialog,
										int keyCode, KeyEvent event) {
									if (keyCode == KeyEvent.KEYCODE_SEARCH
											&& event.getRepeatCount() == 0) {
										return true; // Pretend we processed it
									}
									return false; // Any other keys are still
													// processed
													// as normal
								}
							});
					SessionEvents.onLogoutBegin();
					AsyncFacebookRunner asyncRunner = new AsyncFacebookRunner(
							mFacebook);
					asyncRunner.logout(getApplicationContext(),
							new LogoutRequestListener());
				}
			}
		});
		findViewById(R.id.headerhomeicon).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {}
				});
	}

	private void hideOnScreenMesg() {
		TextView emptyAlbumListText = (TextView) findViewById(R.id.no_albums);
		emptyAlbumListText.setVisibility(View.GONE);
		albumsList.setVisibility(View.VISIBLE);
	}

	private void showOnScreenMesg() {
		TextView emptyAlbumListText = (TextView) findViewById(R.id.no_albums);
		emptyAlbumListText.setVisibility(View.VISIBLE);
		ListView albumList = (ListView) findViewById(R.id.albums_list);
		albumList.setVisibility(View.GONE);
	}

	private class LoadApplicationInformation extends
			AsyncTask<String, Boolean, Boolean> {
		ProgressDialog mProgDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (!Common.isInternetAvailable(FBFriendsActivity.this)
					|| Common.isAirplaneModeOn(FBFriendsActivity.this)) {
				internetAlertMsg(FBFriendsActivity.this);
				mIsAlertDisplayed = true;
				return;
			}
			mProgDialog = ProgressDialog.show(FBFriendsActivity.this,"&&&&&&&&&&&&&","*&%&HVJJJ", false, false);
			mProgDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
				@Override
				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_SEARCH
							&& event.getRepeatCount() == 0) {
						return true; // Pretend we processed it
					}
					return false; // Any other keys are still processed
									// as normal
				}
			});
		}

		@Override
		protected Boolean doInBackground(String... params) {

			if (mFacebook.isSessionValid()) {
				try {

					String response = new FBConnectQueryProcessor()
							.getApplicationInfo(getApplicationContext(),
									mFacebook, "193498163996120");
					if (Common.DEBUG) {
						Log.d(FBFriendsActivity.this.getClass().getName(),
								"Application Info Response : " + response);
					}
					JSONObject mainObj = new JSONObject(response);
					if (mainObj.has("error")) {
						mainObj = null;
						return false;
					} else {
						if (mainObj.has("data")) {
							fbApplicationInfoBean = new FBApplicationInfoBean();
							JSONArray jsonArray = mainObj.getJSONArray("data");
							JSONObject jsonObject = jsonArray.getJSONObject(0);
							if (jsonObject.has("display_name")) {
								fbApplicationInfoBean
										.setApplicationDisplayName(jsonObject
												.getString("display_name")
												+ " Photos");
							}
							if (jsonObject.has("namespace")) {
								fbApplicationInfoBean
										.setApplicationNameSpace(jsonObject
												.getString("namespace")
												+ " Photos");
							}
							if (jsonObject.has("app_id")) {
								fbApplicationInfoBean
										.setApplicationID(jsonObject
												.getString("app_id"));
							}
						}
						mainObj = null;
					}
					return true;
				} catch (Exception exception) {
					return false;
				}
			} else {
				return false;
			}

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (mProgDialog != null && mProgDialog.isShowing())
				mProgDialog.dismiss();
			if (fbApplicationInfoBean != null) {
				new LoadFBFriendsListTask().execute(null, null, null);
			} else {
				callServiceUnAvailMesg();
			}

		}
	}

	private class LoadFBFriendsListTask extends
			AsyncTask<String, Boolean, Boolean> {
		ProgressDialog mProgDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (!Common.isInternetAvailable(FBFriendsActivity.this)
					|| Common.isAirplaneModeOn(FBFriendsActivity.this)) {
				internetAlertMsg(FBFriendsActivity.this);
				mIsAlertDisplayed = true;
				return;
			}
			mProgDialog = ProgressDialog.show(FBFriendsActivity.this,
					getString(R.string.progress_default_title),
					getString(R.string.progress_rx_msg), false, false);
			mProgDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
				@Override
				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_SEARCH
							&& event.getRepeatCount() == 0) {
						return true; // Pretend we processed it
					}
					return false; // Any other keys are still processed
									// as normal
				}
			});
		}

		@Override
		protected Boolean doInBackground(String... params) {
			if (mFacebook.isSessionValid()) {
				try {
					String response = new FBConnectQueryProcessor()
							.requestFriendsList(getApplicationContext(),
									mFacebook);
					JSONObject mainObj = new JSONObject(response);
					if (mainObj.has("error")) {
						mainObj = null;
						return false;
					}else{
						mainObj = null;
					}
					parseFBFriendResponse(response);
					return true;
				} catch (Exception e) {
					return false;
				}
			} else {
				return false;
			}
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			if (mProgDialog != null && mProgDialog.isShowing()) {
				mProgDialog.dismiss();
				mProgDialog = null;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (mProgDialog != null && mProgDialog.isShowing()) {
				mProgDialog.dismiss();
				mProgDialog = null;
			}
			if (!result) {
				callServiceUnAvailMesg();
				return;
			}
			if (listFbFriendInfoBean.size() > 0) {
				hideOnScreenMesg();
				Collections.sort(listFbFriendInfoBean, new Comparator<FbFriend>() {

					@Override
					public int compare(FbFriend lhs, FbFriend rhs) {
						
						return lhs.getName().compareToIgnoreCase(rhs.getName());
					}
				});
				MyFBFriendAdapter fbAdapter =	new MyFBFriendAdapter(
						FBFriendsActivity.this,R.layout.layout_row, 
						listFbFriendInfoBean, urlImageLoader);
				
				albumsList.setAdapter(fbAdapter);
				/*albumsList.setAdapter(new FBFriendsListAdapter(
						FBFriendsActivity.this, R.layout.fb_friend_row,
						listFbFriendInfoBean, urlImageLoader));*/
			        SideBar indexBar = (SideBar) findViewById(R.id.sideBar1);  
			        indexBar.setListView(albumsList);
				  
			} else {
				showOnScreenMesg();
			}
		

		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mProgressDialog != null) {
			if (mProgressDialog.isShowing())
				mProgressDialog.dismiss();
		}
		urlImageLoader.recycleBitmaps();
		urlImageLoader.clearCache();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		// fBConnectImageLoader.clearCache();
	}

	private void showAlert(Context context, String Title, String Message) {
		AlertDialog.Builder alertbox = new AlertDialog.Builder(context);
		alertbox.setTitle(Title);
		alertbox.setMessage(Message);
		alertbox.setCancelable(false);
		alertbox.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				saveRunOncePref(FBFriendsActivity.this, 1, "1");

			}
		});
		alertbox.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_SEARCH
						&& event.getRepeatCount() == 0) {
					return true; // Pretend we processed it
				}
				return false;
			}
		});

		alertbox.show();
	}

	public class FacebookAuthListener implements AuthListener {

		public void onAuthSucceed() {
		}

		public void onAuthFail(String error) {
		}
	}

	public class FacebookLogoutListener implements LogoutListener {
		public void onLogoutBegin() {

		}

		public void onLogoutFinish() {
			finish();
		}
	}

	String fetchRunOncePref(Context context, int key) {
		SharedPreferences prefs = context.getSharedPreferences(RUN_ONCE, 0);
		String runOnce = prefs.getString(RUN_ONCE + key, null);
		if (runOnce != null) {
			return runOnce;
		} else {
			return null;
		}
	}

	void saveRunOncePref(Context context, int key, String pRun) {
		SharedPreferences.Editor prefs = context.getSharedPreferences(RUN_ONCE,
				0).edit();
		prefs.putString(RUN_ONCE + key, pRun);
		prefs.commit();
	}

	private void parseFBFriendResponse(String response) throws Exception {
		listFbFriendInfoBean = new ArrayList<FbFriend>();
		JSONObject jsonObject = new JSONObject(response);
		if (jsonObject.has("data")) {
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			if (jsonArray.length() > 0) {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject object = jsonArray.getJSONObject(i);
					mFbFriendInfoBean = new FbFriend();
					if (object.has("uid")) {
						mFbFriendInfoBean.setObjectID(object.getString("uid"));
					}
					if (object.has("name")) {
						mFbFriendInfoBean.setName(object.getString("name"));
					}
					if (object.has("pic")) {
						mFbFriendInfoBean.setUrl(object.getString("pic"));
					}
					
					listFbFriendInfoBean.add(mFbFriendInfoBean);
				}
			}
		}
	}

	private class LogoutRequestListener implements RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			// callback should be run in the original thread,
			// not the background thread
			FBFriendsActivity.this.runOnUiThread(new Runnable() {
				public void run() {
					mProgressDialog.dismiss();
				}
			});
			mHandler.post(new Runnable() {
				public void run() {
					SessionEvents.onLogoutFinish();
					SessionStore.clear(getApplicationContext());
					FBFriendsActivity.this.finish();
				}
			});

		}

		@Override
		public void onIOException(IOException e, Object state) {
			if (Common.DEBUG) {
				Log.e(FBFriendsActivity.this.getClass().getSimpleName(),
						e.getMessage()+"");
			}

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			if (Common.DEBUG) {
				Log.e(FBFriendsActivity.this.getClass().getSimpleName(),
						e.getMessage()+"");
			}
		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			if (Common.DEBUG) {
				Log.e(FBFriendsActivity.this.getClass().getSimpleName(),
						e.getMessage()+"");
			}
		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {
			if (Common.DEBUG) {
				Log.e(FBFriendsActivity.this.getClass().getSimpleName(),
						e.getMessage()+"");
			}
		}

	}

	private void initializeDataBundle(Bundle bundle) {
		if (bundle != null) {
			mFacebookUserInfoBean = (FacebookUserInfoBean) bundle
					.get("FacebookUserInfoBean");
		}
		fbUserName.setText(mFacebookUserInfoBean.getUserProfileName());
		profilePicture.setTag(mFacebookUserInfoBean.getUserImageURL());
		urlImageLoader.DisplayImage(mFacebookUserInfoBean.getUserImageURL(),
				FBFriendsActivity.this, profilePicture);
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if ((keyCode == KeyEvent.KEYCODE_BACK)) {
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	private void callServiceUnAvailMesg() {
		if (!mIsAlertDisplayed) {
			showServiceUnAvailbleDialog(FBFriendsActivity.this,
					getString(R.string.alert_InternetConnection),
					getString(R.string.progress_rx_msg));
		}
	}

	private void showServiceUnAvailbleDialog(Context context, String Title,
			String Message) {
		mIsAlertDisplayed = true;
		AlertDialog.Builder serviceUnAvailAlertbox = new AlertDialog.Builder(
				context);
		serviceUnAvailAlertbox.setTitle(Title);
		serviceUnAvailAlertbox.setMessage(Message);
		serviceUnAvailAlertbox.setCancelable(false);
		serviceUnAvailAlertbox.setPositiveButton("OK",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						FBFriendsActivity.this.finish();
					}
				});
		serviceUnAvailAlertbox.setOnKeyListener(new OnKeyListener() {

			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_SEARCH
						&& event.getRepeatCount() == 0) {
					return true; // Pretend we processed it
				}
				return false;
			}
		});
		serviceUnAvailAlertbox.show();
	}

    public  void internetAlertMsg(Context ctx) {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(ctx);
        alertbox.setTitle(R.string.alert_InternetConnection_title);
        alertbox.setMessage(R.string.alert_InternetConnection);
        alertbox.setNeutralButton("OK",new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				navigateToPhotoLanding();
			}
		});
        alertbox.show();
    }
	private void navigateToPhotoLanding() {/*
		Intent i = new Intent(FBConnectActivity.this, PhotoLanding.class);
		i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);
		finish();

	*/}
}
