package com.facebook.samples.fbconnect;

import java.io.Serializable;

public class FacebookImageSizeInfoBean implements Serializable {
	//private String imageID;
	private String imageSource;
	private int imageHeight;
	private int imageWidth;
	/**
	 * @return the imageID
	 */
//	public String getImageID() {
//		return imageID;
//	}

	/**
	 * @param imageID
	 *            the imageID to set
	 */
//	public void setImageID(String imageID) {
//		this.imageID = imageID;
//	}

	/**
	 * @return the imageSource
	 */
	public String getImageSource() {
		return imageSource;
	}

	/**
	 * @param imageSource
	 *            the imageSource to set
	 */
	public void setImageSource(String imageSource) {
		this.imageSource = imageSource;
	}

	/**
	 * @return the imageHeight
	 */
	public int getImageHeight() {
		return imageHeight;
	}

	/**
	 * @param imageHeight the imageHeight to set
	 */
	public void setImageHeight(int imageHeight) {
		this.imageHeight = imageHeight;
	}

	/**
	 * @return the imageWidth
	 */
	public int getImageWidth() {
		return imageWidth;
	}

	/**
	 * @param imageWidth the imageWidth to set
	 */
	public void setImageWidth(int imageWidth) {
		this.imageWidth = imageWidth;
	}
}
