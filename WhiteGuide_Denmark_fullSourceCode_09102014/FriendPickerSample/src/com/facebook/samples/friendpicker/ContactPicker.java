package com.facebook.samples.friendpicker;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.samples.contacts.PhoneBookContactsAdapter;
import com.facebook.samples.contacts.PhoneContact;
import com.facebook.samples.contacts.SideBar;
import com.facebook.samples.facebook.AsyncFacebookRunner;
import com.facebook.samples.facebook.AsyncFacebookRunner.RequestListener;
import com.facebook.samples.facebook.DialogError;
import com.facebook.samples.facebook.Facebook;
import com.facebook.samples.facebook.Facebook.DialogListener;
import com.facebook.samples.facebook.FacebookError;
import com.facebook.samples.facebook.SessionEvents;
import com.facebook.samples.facebook.SessionEvents.AuthListener;
import com.facebook.samples.facebook.SessionEvents.LogoutListener;
import com.facebook.samples.facebook.SessionStore;
import com.facebook.samples.facebook.Util;
import com.facebook.samples.fbconnect.FBConnectQueryProcessor;
import com.facebook.samples.fbconnect.FacebookUserInfoBean;
import com.facebook.samples.fbconnect.FbFriend;
import com.facebook.samples.fbconnect.MyFBFriendAdapter;

public class ContactPicker extends Activity {

	private Facebook mFacebook;
	private AsyncFacebookRunner mAsyncFacebookRunner;
	private FacebookUserInfoBean mFacebookUserInfoBean;
	private ProgressDialog mProgressDialog = null;
	private Handler mHandler;
	String contactName, contactTelNumber = "";
	String contactID;
	public static List<PhoneContact> stringList;
	private HashMap<String, PhoneContact> nameMap;
	private boolean mIsAlertDisplayed;
	ListView listView;
	SideBar indexBar;
	private FbFriend mFbFriendInfoBean;
	private List<FbFriend> listFbFriendInfoBean;
	private UrlImageLoader urlImageLoader;
	Button fbContacts, phoneContact;
	private boolean isContact;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		fbContacts = (Button) findViewById(R.id.fb_Friends);
		phoneContact = (Button) findViewById(R.id.phone_contacts);
		urlImageLoader = new UrlImageLoader(ContactPicker.this, true);
		stringList = new ArrayList<PhoneContact>();
		nameMap = new HashMap<String, PhoneContact>();
		listView = (ListView) findViewById(R.id.myListView);
		indexBar = (SideBar) findViewById(R.id.sideBar);
		doFetchTheContacts();
		isContact = true;
		fbContacts.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (listFbFriendInfoBean != null
						&& listFbFriendInfoBean.size() > 0) {

					MyFBFriendAdapter fbAdapter = new MyFBFriendAdapter(
							ContactPicker.this, R.layout.layout_row,
							listFbFriendInfoBean, urlImageLoader);

					listView.setAdapter(fbAdapter);
					indexBar.setListView(listView);

				} else {
					callPrintFromFacebook();
				}
				isContact = false;
				setBackGrounds();
			}

		});
		phoneContact.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (stringList != null && stringList.size() > 0) {
					PhoneBookContactsAdapter adapter = new PhoneBookContactsAdapter(
							ContactPicker.this, stringList);
					listView.setAdapter(adapter);
					indexBar.setListView(listView);
				} else {
					doFetchTheContacts();
				}

				isContact = true;
				setBackGrounds();
			}

		});
		// setContentView(b);
	}

	protected void setBackGrounds() {
		if (isContact) {
			fbContacts.setBackgroundResource(R.drawable.fb_tab_unselected);
			phoneContact.setBackgroundResource(R.drawable.fb_tab_selected);
		} else {
			fbContacts.setBackgroundResource(R.drawable.fb_tab_selected);
			phoneContact.setBackgroundResource(R.drawable.fb_tab_unselected);
		}

	}

	private void doFetchTheContacts() {
		Cursor c = getContentResolver().query(
				ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
		int idIndex = c.getColumnIndex(ContactsContract.Contacts._ID);
		int hasNumberIndex = c
				.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER);
		int nameIndex = c
				.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME);
		int photoIndex = c
				.getColumnIndex(ContactsContract.Contacts.PHOTO_THUMBNAIL_URI);
		while (c.moveToNext()) {
			contactName = c.getString(nameIndex);
			contactID = c.getString(idIndex);
			// If this is an integer ask for an integer
			if (c.getInt(hasNumberIndex) > 0) {
				Cursor pCur = getContentResolver().query(
						ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
						null,
						ContactsContract.CommonDataKinds.Phone.CONTACT_ID
								+ " = ?", new String[] { contactID }, null);
				while (pCur.moveToNext()) {
					contactTelNumber = pCur
							.getString(pCur
									.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					PhoneContact contact = new PhoneContact();
					contact.setContactName(contactName);
					contact.setContactNumber(contactTelNumber);
					contact.setContactUri(c.getString(photoIndex));
					if (nameMap.containsKey(contactName)) {
						PhoneContact dummyContact = nameMap.get(contactName);
						if (!dummyContact.getExtraNums().contains(
								contactTelNumber)) {
							dummyContact.getExtraNums().add(contactTelNumber);
						}
					} else {
						contact.getExtraNums().add(contactTelNumber);
						nameMap.put(contactName, contact);
					}
					// Store the "name: number" string in our list
					// stringList.add(contact);
				}
			}
		}
		stringList.addAll(nameMap.values());
		Collections.sort(stringList, new Comparator<PhoneContact>() {

			@Override
			public int compare(PhoneContact p1, PhoneContact p2) {
				return p1.getContactName().compareToIgnoreCase(
						p2.getContactName());
			}
		});
		// Find the ListView, create the adapter, and bind them
		PhoneBookContactsAdapter adapter = new PhoneBookContactsAdapter(
				ContactPicker.this, stringList);
		listView.setAdapter(adapter);
		indexBar.setListView(listView);
	}

	private void callPrintFromFacebook() {
		if (!Common.isInternetAvailable(ContactPicker.this)
				|| Common.isAirplaneModeOn(ContactPicker.this)) {
			Common.internetAlertMsg(ContactPicker.this);
			return;
		}
		mFacebook = new Facebook("193498163996120");
		mHandler = new Handler();
		mAsyncFacebookRunner = new AsyncFacebookRunner(mFacebook);
		SessionStore.restore(mFacebook, getApplicationContext());
		SessionEvents.addAuthListener(new FacebookAuthListener());
		SessionEvents.addLogoutListener(new FacebookLogoutListener());
		if (mFacebook.isSessionValid()) {
			mProgressDialog = ProgressDialog.show(ContactPicker.this, "Laddar",
					"V�nta...", false, false);
			mProgressDialog
					.setOnKeyListener(new DialogInterface.OnKeyListener() {
						@Override
						public boolean onKey(DialogInterface dialog,
								int keyCode, KeyEvent event) {
							if (keyCode == KeyEvent.KEYCODE_SEARCH
									&& event.getRepeatCount() == 0) {
								return true; // Pretend we processed it
							}
							return false; // Any other keys are still processed
							// as normal
						}
					});
			mAsyncFacebookRunner
					.request("me", new UserDetailsRequestListener());
		} else {
			mFacebook.authorize(ContactPicker.this, Util.FACEBOOK_PERMISSIONS,
					Facebook.FORCE_DIALOG_AUTH,
					new FacebookLoginDialogListener());
		}
	}

	public class FacebookLogoutListener implements LogoutListener {
		public void onLogoutBegin() {

		}

		public void onLogoutFinish() {

		}
	}

	class FacebookLoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionEvents.onLoginSuccess();
			mAsyncFacebookRunner
					.request("me", new UserDetailsRequestListener());
			SessionStore.save(mFacebook, getApplicationContext());
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {
			SessionEvents.onLoginError("Action Canceled");
		}
	}

	public class FacebookAuthListener implements AuthListener {

		public void onAuthSucceed() {
			SessionStore.save(mFacebook, getApplicationContext());
		}

		public void onAuthFail(String error) {
		}
	}

	private class UserDetailsRequestListener implements DialogListener,
			RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			mFacebookUserInfoBean = new FacebookUserInfoBean();
			try {
				JSONObject json = Util.parseJson(response);
				mFacebookUserInfoBean.setUserID(json.getString("id"));
				mFacebookUserInfoBean.setUserImageURL(Util.IMAGELINK
						+ json.getString("id") + Util.NORMAL_SIZE_PIC);
				mFacebookUserInfoBean.setUserName(json.getString("username"));
				mFacebookUserInfoBean
						.setUserProfileName(json.getString("name"));
				ContactPicker.this.runOnUiThread(new Runnable() {
					public void run() {
						if (mProgressDialog != null
								&& mProgressDialog.isShowing())
							mProgressDialog.dismiss();
						new LoadFBFriendsListTask().execute(null, null, null);
					}
				});

			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FacebookError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onIOException(IOException e, Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {
			Toast.makeText(getApplicationContext(),
					"Authentication with Facebook failed!", 10).show();
		}

		@Override
		public void onComplete(Bundle values) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onFacebookError(FacebookError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onError(DialogError e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void onCancel() {
			// TODO Auto-generated method stub

		}

	}

	private class LoadFBFriendsListTask extends
			AsyncTask<String, Boolean, Boolean> {
		ProgressDialog mProgDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (!Common.isInternetAvailable(ContactPicker.this)
					|| Common.isAirplaneModeOn(ContactPicker.this)) {
				internetAlertMsg(ContactPicker.this);
				mIsAlertDisplayed = true;
				return;
			}
			mProgDialog = ProgressDialog.show(ContactPicker.this,
					getString(R.string.progress_default_title),
					getString(R.string.progress_rx_msg), false, false);
			mProgDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
				@Override
				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_SEARCH
							&& event.getRepeatCount() == 0) {
						return true; // Pretend we processed it
					}
					return false; // Any other keys are still processed
									// as normal
				}
			});
		}

		@Override
		protected Boolean doInBackground(String... params) {
			if (mFacebook.isSessionValid()) {
				try {
					String response = new FBConnectQueryProcessor()
							.requestFriendsList(getApplicationContext(),
									mFacebook);
					JSONObject mainObj = new JSONObject(response);
					if (mainObj.has("error")) {
						mainObj = null;
						return false;
					} else {
						mainObj = null;
					}
					parseFBFriendResponse(response);
					return true;
				} catch (Exception e) {
					return false;
				}
			} else {
				return false;
			}
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
			if (mProgDialog != null && mProgDialog.isShowing()) {
				mProgDialog.dismiss();
				mProgDialog = null;
			}
		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (mProgDialog != null && mProgDialog.isShowing()) {
				mProgDialog.dismiss();
				mProgDialog = null;
			}
			if (!result) {
				callServiceUnAvailMesg();
				return;
			}
			if (listFbFriendInfoBean.size() > 0) {
				hideOnScreenMesg();
				Collections.sort(listFbFriendInfoBean,
						new Comparator<FbFriend>() {

							@Override
							public int compare(FbFriend lhs, FbFriend rhs) {

								return lhs.getName().compareToIgnoreCase(
										rhs.getName());
							}
						});
				MyFBFriendAdapter fbAdapter = new MyFBFriendAdapter(
						ContactPicker.this, R.layout.layout_row,
						listFbFriendInfoBean, urlImageLoader);

				listView.setAdapter(fbAdapter);
				indexBar.setListView(listView);

			} else {
				showOnScreenMesg();
			}

		}

		private void callServiceUnAvailMesg() {
			if (!mIsAlertDisplayed) {
				showServiceUnAvailbleDialog(ContactPicker.this,
						getString(R.string.alert_InternetConnection),
						getString(R.string.progress_rx_msg));
			}
		}

		private void showServiceUnAvailbleDialog(Context context, String Title,
				String Message) {
			mIsAlertDisplayed = true;
			AlertDialog.Builder serviceUnAvailAlertbox = new AlertDialog.Builder(
					context);
			serviceUnAvailAlertbox.setTitle(Title);
			serviceUnAvailAlertbox.setMessage(Message);
			serviceUnAvailAlertbox.setCancelable(false);
			serviceUnAvailAlertbox.setPositiveButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							ContactPicker.this.finish();
						}
					});
			serviceUnAvailAlertbox.setOnKeyListener(new OnKeyListener() {

				@Override
				public boolean onKey(DialogInterface dialog, int keyCode,
						KeyEvent event) {
					if (keyCode == KeyEvent.KEYCODE_SEARCH
							&& event.getRepeatCount() == 0) {
						return true; // Pretend we processed it
					}
					return false;
				}
			});
			serviceUnAvailAlertbox.show();
		}

		public void internetAlertMsg(Context ctx) {
			AlertDialog.Builder alertbox = new AlertDialog.Builder(ctx);
			alertbox.setTitle(R.string.alert_InternetConnection_title);
			alertbox.setMessage(R.string.alert_InternetConnection);
			alertbox.setNeutralButton("OK",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
						}
					});
			alertbox.show();
		}

		private void hideOnScreenMesg() {
			TextView emptyAlbumListText = (TextView) findViewById(R.id.no_albums);
			emptyAlbumListText.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
		}

		private void showOnScreenMesg() {
			TextView emptyAlbumListText = (TextView) findViewById(R.id.no_albums);
			emptyAlbumListText.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
		}
	}

	private void parseFBFriendResponse(String response) throws Exception {
		listFbFriendInfoBean = new ArrayList<FbFriend>();
		JSONObject jsonObject = new JSONObject(response);
		if (jsonObject.has("data")) {
			JSONArray jsonArray = jsonObject.getJSONArray("data");
			if (jsonArray.length() > 0) {
				for (int i = 0; i < jsonArray.length(); i++) {
					JSONObject object = jsonArray.getJSONObject(i);
					mFbFriendInfoBean = new FbFriend();
					if (object.has("uid")) {
						mFbFriendInfoBean.setObjectID(object.getString("uid"));
					}
					if (object.has("name")) {
						mFbFriendInfoBean.setName(object.getString("name"));
					}
					if (object.has("pic")) {
						mFbFriendInfoBean.setUrl(object.getString("pic"));
					}

					listFbFriendInfoBean.add(mFbFriendInfoBean);
				}
			}
		}
	}

}
