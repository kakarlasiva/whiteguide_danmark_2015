package com.facebook.samples.contacts;

import java.util.ArrayList;
import java.util.List;

public class PhoneContact {

	private String contactName;
	private String contactNumber;
	private String contactUri;
	private List<String> extraNums = new ArrayList<String>(); 

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getContactUri() {
		return contactUri;
	}

	public void setContactUri(String contactUri) {
		this.contactUri = contactUri;
	}

	public List<String> getExtraNums() {
		return extraNums;
	}

	public void setExtraNums(List<String> extraNums) {
		this.extraNums = extraNums;
	}

}
