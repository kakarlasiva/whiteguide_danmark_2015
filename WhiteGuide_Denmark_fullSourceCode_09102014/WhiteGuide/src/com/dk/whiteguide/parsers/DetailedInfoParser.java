package com.dk.whiteguide.parsers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONObject;

import com.dk.whiteguide.dataengine.DataEngine;
import com.dk.whiteguide.dataengine.Restaurant;
import com.dk.whiteguide.dataengine.TagConstants;

public class DetailedInfoParser {
	// stream corresponding to the given url.
	InputStream in;

	public void doItforInidividualThi(Restaurant restaurant) {
		// TODO Auto-generated method stub
		HttpURLConnection http = null;
		URL url = null;
		try {
			url = new URL(DataEngine.BASE_URL + "restaurants/"
					+ restaurant.getId() + ".json");

		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			http = (HttpURLConnection) url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			in = new BufferedInputStream(http.getInputStream());
		} catch (Exception e) {
			// TODO: handle exception
		}
		String jsonResponse = getStringFromInputStream(in);
		try {
			JSONObject json = new JSONObject(jsonResponse);
			restaurant.setAddress(json.getString(TagConstants.ADDRESS));
			restaurant.setCity((json.getString(TagConstants.CITY)));
			restaurant.setPhone(json.getString(TagConstants.PHONE));
			restaurant.setZip(json.getString(TagConstants.ZIP));
			restaurant.setWebsite(json.getString(TagConstants.WEBSITE));
			restaurant.setOpen(json.getString(TagConstants.OPEN));
			restaurant.setProvince(json.getString(TagConstants.PROVINCE));
			restaurant.setBox(json.getString(TagConstants.BOX));
			restaurant.setEmail(json.getString(TagConstants.EMAIL));
			restaurant.setDirections(json.getString(TagConstants.DIRECTIONS));
			restaurant.setComment_count(json
					.getString(TagConstants.COMMENT_COUNT));
			restaurant.setLast_comment(json
					.getString(TagConstants.LAST_COMMENT));
			restaurant.setOwner(TagConstants.OWNER);
			restaurant.setBarkeeper(TagConstants.BARKEEPER);
			restaurant.setManager(TagConstants.MANAGER);
			restaurant.setChief_chef(TagConstants.CHIEF_CHEF);
			restaurant.setSommelier(TagConstants.SOMMELIER);
			restaurant.setNumber_of_seats(TagConstants.NUMBER_OF_SEATS);
			restaurant.setCheapest_entre(TagConstants.CHEAPEST_ENTRE);
			restaurant.setExpensive_entre(TagConstants.EXPENSIV_ENTRE);
			restaurant.setCheapest_main_dish(TagConstants.CHEAPEST_MAIN_DISH);
			restaurant.setExpensive_main_dish(TagConstants.EXPENSIVE_MAIN_DISH);
			restaurant.setCheapest_dessert(TagConstants.CHEAPEST_DESERT);
			restaurant.setExpensive_dessert(TagConstants.EXPENSIVE_DESERT);
			restaurant.setCheapest_menu(TagConstants.CHEAPEST_MENU);
			restaurant.setExpensive_menu(TagConstants.EXPENSIVE_MENU);
			restaurant.setIndex(TagConstants.INDEX);
			restaurant.setPosition(TagConstants.POSITION);
			// need to add pictures && symbols...
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	// convert InputStream to String
	private static String getStringFromInputStream(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();

	}
}
