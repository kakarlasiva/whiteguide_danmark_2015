package com.dk.whiteguide.tabgroup;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.dk.whiteguide.screens.StoreActivity;

/**
 * Represnts the views for the payments of the guides.
 * @author Conevo
 */
public class SettingsGroup extends TabGroupActivity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Hide the Title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		startChildActivity("SettingsActivity", new Intent(this,
				StoreActivity.class));
	}

	@SuppressWarnings("deprecation")
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		try {
			if (intent != null) {
				StoreActivity activity = (StoreActivity) getLocalActivityManager()
						.getCurrentActivity();
				activity.onActivityResult(requestCode, resultCode, intent);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
