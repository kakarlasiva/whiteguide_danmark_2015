package com.dk.whiteguide.tabgroup;

import android.content.Intent;
import android.os.Bundle;
import android.view.Window;

import com.dk.whiteguide.RestaurantsList;

/**
 * Represents the views for the Restaurant/Caf�er 
 * @author Conevo
 */
public class ListGroup extends TabGroupActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Hide the Title
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		Intent intent = new Intent(this, RestaurantsList.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable("INFO", Home.beanObj);
		intent.putExtras(bundle);
		startChildActivity("ListActivity", intent);
	}
	/**
	 *   Called from Home for refreshing the view when clicking on Restaurant.
	 */
	public void doHack(){
		Intent intent = new Intent(this, RestaurantsList.class);
		Bundle bundle = new Bundle();
		bundle.putSerializable("INFO", Home.beanObj);
		intent.putExtras(bundle);
		startChildActivity("ListActivity", intent);
	}
	
}
