package com.dk.whiteguide.tabgroup;

import java.util.ArrayList;

import android.app.Activity;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.app.LocalActivityManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Window;

import com.dk.whiteguide.MainActivity;
import com.dk.whiteguide.MapFragmentCluster;
import com.live.dk.whiteguide.R;


/**
 * Represents the view for the Mapview of the Restaurents and caf�er 
 * @author Conevo
 *
 */

@SuppressWarnings("deprecation")
public class MapGroup extends ActivityGroup {

	private ArrayList<String> mIdList;
	static boolean list_flag = false;

	
	@Override
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		super.onNewIntent(intent);
	}

	/**
	 *  Shows the alert of application exit for the user
	 */
	private void set_alertdialog(Context mtx) {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getParent());
		builder.setMessage(getString(R.string.doyouwant_close));
		builder.setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				finish();
			}
		});
		builder.setNegativeButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
		builder.show();

	}

	/**
	 * This is called when a child activity of this one calls its finish method.
	 * This implementation calls {@link LocalActivityManager#destroyActivity} on
	 * the child activity and starts the previous activity. If the last child
	 * activity just called finish(),this activity (the parent), calls finish to
	 * finish the entire group.
	 */
	@Override
	public void finishFromChild(Activity child) {
		LocalActivityManager manager = getLocalActivityManager();
		int index = mIdList.size() - 1;
		if (index < 1) {
			set_alertdialog(this);
			return;
		}
		manager.destroyActivity(mIdList.get(index), true);
		mIdList.remove(index);
		index--;
		String lastId = mIdList.get(index);
		try {
			setContentView(manager.getActivity(lastId).getWindow().getDecorView());
		} catch (Exception e) {
			// finish();
		}
	}

	/**
	 * Starts an Activity as a child Activity to this.
	 * 
	 * @param Id
	 *            Unique identifier of the activity to be started.
	 * @param intent
	 *            The Intent describing the activity to be started.
	 * @throws android.content.ActivityNotFoundException.
	 */
	Window window;
	public void startChildActivity(String Id, Intent intent) {
		window = getWindow();
		window = getLocalActivityManager().startActivity(Id,
				intent);//.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
		if (window != null) {
			if (list_flag) {
				list_flag = false;
				mIdList = new ArrayList<String>();
			}
			mIdList.add(Id);
			setContentView(window.getDecorView());
		}
	}

	/**
	 * The primary purpose is to prevent systems before
	 * android.os.Build.VERSION_CODES.ECLAIR from calling their default
	 * KeyEvent.KEYCODE_BACK during onKeyDown.
	 */
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			// preventing default implementation previous to
			return true;
		}
		return super.onKeyDown(keyCode, event);
	}

	/**
	 * Overrides the default implementation for KeyEvent.KEYCODE_BACK so that
	 * all systems call onBackPressed().
	 */
	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			onBackPressed();
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	/**
	 * If a Child Activity handles KeyEvent.KEYCODE_BACK. Simply override and
	 * add this method.
	 */
	@Override
	public void onBackPressed() {
		int length = mIdList.size();
		if (length > 1) {
			try {
				Activity current = getLocalActivityManager().getActivity(
						mIdList.get(length - 1));
				current.finish();
			} catch (Exception e) {
				// TODO: handle exception
				set_alertdialog(this);
			}
		} else {
			set_alertdialog(this);
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		if (mIdList == null)
			mIdList = new ArrayList<String>();
		Intent intent = new Intent(this, MapFragmentCluster.class);
		startChildActivity("OptionsActivity", intent);
	}
}
