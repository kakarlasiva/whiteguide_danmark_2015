package com.dk.whiteguide.screens;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.dk.whiteguide.RestaurantsList;
import com.dk.whiteguide.dataengine.DataEngine;
import com.dk.whiteguide.dataengine.DataHandler;
import com.dk.whiteguide.dataengine.PaymentInformation;
import com.dk.whiteguide.dataengine.Restaurant;
import com.dk.whiteguide.helpers.AZRestaurantsAdapter;
import com.dk.whiteguide.helpers.BasicInfoBean;
import com.dk.whiteguide.helpers.CategoryRestaurantAdapter;
import com.dk.whiteguide.helpers.SideBar;
import com.dk.whiteguide.helpers.SoftKeyboardHandledLinearLayout;
import com.dk.whiteguide.helpers.SoftKeyboardHandledLinearLayout.SoftKeyboardVisibilityChangeListener;
import com.dk.whiteguide.parsers.RestaurantsParser;
import com.dk.whiteguide.tabgroup.Home;
import com.dk.whiteguide.tabgroup.ListGroup;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.live.dk.whiteguide.R;


/**
 * @author Conevo
 *   Represents the view after the filtering the list with the search option.
 */
public class FilteredRestaurants extends Activity {
	ListView list;
	EditText search;
	Button a_z, cities, categories;
	SideBar sideBar;
	Vector<Restaurant> azRestaurants, categoryRestaurants, totalRestaurants;
	AZRestaurantsAdapter adapter;
	CategoryRestaurantAdapter catgAdapter;
	PaymentInformation info;
	boolean iskeyboardvisible=false;
	String cityName;
	boolean azSelected = false, citiesSelected = false,
			categorySelected = false;
	TextView header_text;
	BasicInfoBean beanObj;
	DataHandler dataHandler;
	private ProgressDialog progress;
	private int brastt_count = 0, godclass_count = 0, interclass_count = 0,
			myketclass_count = 0, masterclas_count = 0;
	private Background background;
	
	AdView adView;

	/**
	 *  Shows the alert to buy the guide if not brought.
	 */
	protected void showAlertBox() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getParent());
		builder.setMessage(getString(R.string.guide_buytxt_bef)+" "+beanObj.getGuidePrice()+" "+getString(R.string.guidetxt_after));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						Home.setTab(3);
					}
				});
		builder.setNegativeButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();

	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.contact_dropdown_list_item);
		SoftKeyboardHandledLinearLayout mainView = (SoftKeyboardHandledLinearLayout) findViewById(R.id.mainView);
		mainView.setOnSoftKeyboardVisibilityChangeListener(
				new SoftKeyboardVisibilityChangeListener() {
					@Override
					public void onSoftKeyboardShow() {
						sideBar.setVisibility(View.INVISIBLE);
						iskeyboardvisible=true;
					}
					
					@Override
					public void onSoftKeyboardHide() {
						sideBar.setVisibility(View.VISIBLE);
						iskeyboardvisible=false;
					}
				});
		info = new PaymentInformation(getApplicationContext());
		beanObj = (BasicInfoBean) getIntent().getSerializableExtra("GUIDE");
		dataHandler = new DataHandler(this.getParent(), beanObj.getGuideName());

		adView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		if (DataEngine.showAds)
			adView.loadAd(adRequest);
		else
			adView.setVisibility(View.GONE);
		progress = new ProgressDialog(this.getParent());
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));

		cityName = getIntent().getExtras().getString("CITY");
		list = (ListView) findViewById(R.id.myListView);
		search = (EditText) findViewById(R.id.searchContacts);
		search.setHint(cityName);
		a_z = (Button) findViewById(R.id.restaurants);
		cities = (Button) findViewById(R.id.cities);
		cities.setVisibility(View.GONE);
		categories = (Button) findViewById(R.id.categories);
		totalRestaurants = new Vector<Restaurant>();
		azRestaurants = new Vector<Restaurant>();
		categoryRestaurants = new Vector<Restaurant>();
		sideBar = (SideBar) findViewById(R.id.sideBar);
		if (isnyanmeldt()) {
			a_z.setText(getString(R.string.senas));
			LinearLayout.LayoutParams a_zparam = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 0.9f);
			a_z.setLayoutParams(a_zparam);
			LinearLayout.LayoutParams citiesparam = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.2f);
			cities.setLayoutParams(citiesparam);
			LinearLayout.LayoutParams catparam = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 0.9f);
			categories.setLayoutParams(catparam);
			sideBar.setVisibility(View.GONE);
		}

		if (null == background) {
			background = new Background();
		} else {
			background.cancel(true);
		}
		background.execute();

	}
	
	public boolean isnyanmeldt() {
		if (beanObj.getGuideName().equalsIgnoreCase(
				this.getString(R.string.nyanmeldt_name))) {
			return true;
		} else {
			return false;
		}
	}

	class Background extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);

			for (Restaurant resta : totalRestaurants) {
				if (resta.getCity() != null
						|| "".equalsIgnoreCase(resta.getCity())) {
					if (resta.getCity().equalsIgnoreCase(cityName)) {
						azRestaurants.add(resta);
						categoryRestaurants.add(resta);
					}
				}
			}

			setSelections(true, false, false);
			setBackgrounds(Color.DKGRAY, Color.LTGRAY, Color.LTGRAY);
			setListeners();
			for (Restaurant temp : categoryRestaurants) {
				if (temp.getRestaurantClass().contains("7")) {
					brastt_count++;
				} else if (temp.getRestaurantClass().contains("2")) {
					masterclas_count++;
				} else if (temp.getRestaurantClass().contains("4")) {
					godclass_count++;
				} else if (temp.getRestaurantClass().contains("3")) {
					myketclass_count++;
				} else if (temp.getRestaurantClass().contains("1")) {
					interclass_count++;
				}
			}
			brastt_count = interclass_count + myketclass_count + godclass_count
					+ masterclas_count;
			godclass_count = interclass_count + myketclass_count
					+ masterclas_count;
			myketclass_count = interclass_count + masterclas_count;
			masterclas_count = interclass_count;
			interclass_count = 0;
			if (isnyanmeldt()) {
				Collections.sort(azRestaurants, new Comparator<Restaurant>() {

					@Override
					public int compare(Restaurant p1, Restaurant p2) {
						return p2.getChanged().compareToIgnoreCase(p1.getChanged());
					}
				});
				

			} else {
			Collections.sort(azRestaurants, new Comparator<Restaurant>() {

				@Override
				public int compare(Restaurant p1, Restaurant p2) {
					return p1.getTitle().compareToIgnoreCase(p2.getTitle());
				}
			});
			}

			Collections.sort(categoryRestaurants, new Comparator<Restaurant>() {

				@Override
				public int compare(Restaurant p1, Restaurant p2) {
					return p1.getRestaurantClass().compareToIgnoreCase(
							p2.getRestaurantClass());
				}
			});

			adapter = new AZRestaurantsAdapter(FilteredRestaurants.this.getParent(),
					R.layout.layout_row, azRestaurants,
					beanObj.isGuideisBought()
							|| info.isThisGuidebought(beanObj.getGuideId(),
									beanObj.getGuideYear()),beanObj);
			catgAdapter = new CategoryRestaurantAdapter(
					FilteredRestaurants.this, R.layout.layout_row,
					categoryRestaurants);
			list.setAdapter(adapter);
			sideBar.setListView(list);
			if (null != progress) {
				if (progress.isShowing())
					progress.dismiss();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			String parse_url=null;
			if (dataHandler.readTreats().size() > 0) {
				totalRestaurants = dataHandler.readTreats();
			} else if (dataHandler.readTempData().size() > 0) {
				totalRestaurants = dataHandler.readTempData();
			} else {
				
				if (beanObj.getGuideName().equalsIgnoreCase(
						FilteredRestaurants.this.getString(R.string.nyanmeldt_name))) {
					parse_url = DataEngine.BASE_URL + DataEngine.nyanmeldt_INFO;
				} else {
					parse_url = DataEngine.BASE_URL
							+ DataEngine.ALL_RESTAURANTS_BASIC_INFO
							+ beanObj.getGuideId();
				}
				RestaurantsParser res_parser = new RestaurantsParser(
						FilteredRestaurants.this,parse_url , beanObj.getGuideName(),
						beanObj.getGuideId(), beanObj.getGuideYear());
				res_parser.doJsonparse();
				totalRestaurants = res_parser.getData();
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			if (null != progress) {
				if (!progress.isShowing())
					progress.show();
			}
			super.onPreExecute();
		}
	}

	/**
	 * Sets the selection flag.
	 * @param azRes
	 * @param citylist
	 * @param cateList
	 */
	private void setSelections(boolean azRes, boolean citylist, boolean cateList) {
		azSelected = azRes;
		citiesSelected = citylist;
		categorySelected = cateList;
	}

	/**
	 * Shows the slider view 
	 * @param v
	 */
	public void calling(View v) {
		Home.setDrawer();
	}

	/**
	 * Sets the listeners for all the class elements touch events
	 */
	public void setListeners() {
		findViewById(R.id.brasst).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				list.setSelection(brastt_count);
			}
		});
		findViewById(R.id.interclass).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				list.setSelection(interclass_count);
			}
		});
		findViewById(R.id.masterclass).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						list.setSelection(masterclas_count);
					}
				});
		findViewById(R.id.godclass).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				list.setSelection(godclass_count);
			}
		});
		findViewById(R.id.myketclass).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				list.setSelection(myketclass_count);
			}
		});
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Home.closeDrawer();
				Intent intent = new Intent(FilteredRestaurants.this,
						DetailedRestaurantview.class);
				if (azSelected)
					intent.putExtra("RES", (Restaurant) arg0.getAdapter()
							.getItem(arg2));
				else
					intent.putExtra("RES", (Restaurant) arg0.getAdapter()
							.getItem(arg2));
				intent.putExtra("GUIDE", beanObj);
				intent.putExtra("PUSH",false);

				ListGroup parentActivity = (ListGroup) getParent();
				parentActivity.startActivity(intent);
				//parentActivity.startChildActivity("restaurantInformation",
						//intent);
			}
		});
		search.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text
				if (azSelected)
				{
					FilteredRestaurants.this.adapter.getFilter().filter(cs);
				}
				if (categorySelected)
				{
					FilteredRestaurants.this.catgAdapter.getFilter().filter(cs);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {
			}
		});
		a_z.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				list.setVisibility(View.VISIBLE);
				list.setAdapter(adapter);
				sideBar.setListView(list);
				adapter.notifyDataSetChanged();
				setSelections(true, false, false);
				setBackgrounds(Color.DKGRAY, Color.LTGRAY, Color.LTGRAY);
				if(!iskeyboardvisible)
				sideBar.setVisibility(View.VISIBLE);
				findViewById(R.id.classifier).setVisibility(View.GONE);
			}
		});

		categories.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				list.setAdapter(catgAdapter);
				sideBar.setListView(list);
				catgAdapter.notifyDataSetChanged();
				setSelections(false, false, true);
				setBackgrounds(Color.LTGRAY, Color.LTGRAY, Color.DKGRAY);
				sideBar.setVisibility(View.GONE);
				if (beanObj.isGuideisBought()
						|| info.isThisGuidebought(beanObj.getGuideId(),
								beanObj.getGuideYear())) {
					findViewById(R.id.classifier).setVisibility(View.VISIBLE);
					list.setVisibility(View.VISIBLE);
				} else {
					findViewById(R.id.classifier).setVisibility(View.GONE);
					list.setVisibility(View.GONE);
					showAlertBox();
				}
			}
		});
	}

	/**
	 * Sets the background colors for the respective buttons.
	 * @param azBackgroud
	 * @param citiesBackgroudn
	 * @param categriesBackground
	 */
	protected void setBackgrounds(int azBackgroud, int citiesBackgroudn,
			int categriesBackground) {
		a_z.setBackgroundColor(azBackgroud);
		cities.setBackgroundColor(citiesBackgroudn);
		categories.setBackgroundColor(categriesBackground);
	}

	class MyAdapter extends BaseAdapter {
		Vector<Restaurant> restaurantsList;
		LayoutInflater inflater;
		Button selection;

		public MyAdapter(Context context, Vector<Restaurant> list,
				Button selector) {
			restaurantsList = new Vector<Restaurant>();
			restaurantsList.addAll(list);
			inflater = LayoutInflater.from(context);
			selection = selector;
		}

		@Override
		public int getCount() {
			return restaurantsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return arg0;
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View convertView, ViewGroup arg2) {
			return convertView;
		}

	}
}
