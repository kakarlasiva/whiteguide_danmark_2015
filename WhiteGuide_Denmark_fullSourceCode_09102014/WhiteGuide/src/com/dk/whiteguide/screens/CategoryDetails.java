package com.dk.whiteguide.screens;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.Window;

import com.live.dk.whiteguide.R;


/**
 * Represents the detailed view of the categories specified.
 * @author Conevo
 *
 */
public class CategoryDetails extends AnimActivity {
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.category_detail);
		findViewById(R.id.back_cat).setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				finish();
				return true;
			}
		});
		findViewById(R.id.header_tab).setOnTouchListener(new OnTouchListener() {

			@Override
			public boolean onTouch(View v, MotionEvent event) {
				finish();
				return true;
			}
		});
	
	}

}
