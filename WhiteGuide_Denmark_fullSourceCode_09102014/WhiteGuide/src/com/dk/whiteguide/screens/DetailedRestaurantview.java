package com.dk.whiteguide.screens;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.text.Spannable;
import android.text.style.URLSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dk.whiteguide.NetworkChecker;
import com.dk.whiteguide.dataengine.DataEngine;
import com.dk.whiteguide.dataengine.FavouritesHandler;
import com.dk.whiteguide.dataengine.InitialInformation;
import com.dk.whiteguide.dataengine.PaymentInformation;
import com.dk.whiteguide.dataengine.Restaurant;
import com.dk.whiteguide.helpers.BasicInfoBean;
import com.dk.whiteguide.helpers.CirclePageIndicator;
import com.dk.whiteguide.tabgroup.Home;
import com.dk.whiteguide.tabgroup.ListGroup;
import com.dk.whiteguide.twitter.Twitt_Sharing;
import com.dk.whiteguide.util.RemoveUnderline;
import com.facebook.samples.facebook.AsyncFacebookRunner;
import com.facebook.samples.facebook.AsyncFacebookRunner.RequestListener;
import com.facebook.samples.facebook.DialogError;
import com.facebook.samples.facebook.Facebook;
import com.facebook.samples.facebook.Facebook.DialogListener;
import com.facebook.samples.facebook.FacebookError;
import com.facebook.samples.facebook.SessionEvents;
import com.facebook.samples.facebook.SessionEvents.AuthListener;
import com.facebook.samples.facebook.SessionEvents.LogoutListener;
import com.facebook.samples.facebook.SessionStore;
import com.facebook.samples.facebook.Util;
import com.facebook.samples.fbconnect.FBConnectQueryProcessor;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.live.dk.whiteguide.R;

/**
 * Represents the detailed view of the particular restaurant click.
 * 
 * @author Conevo
 */
public class DetailedRestaurantview extends AnimActivity {
	TextView description, imagecount, descriptionHeading, descriptionfulltext,
			openingtime, price, phone_id, mail_id, address_id;
	TextView detaildescription;
	GridView grid;
	DisplayMetrics metrics;
	EdgeEffectScrollView scrolldetail;
	Bitmap bitmap;
	ImageView imageView, FavBtnClicked;
	Button gallery;
	ImageView twitter;
	GridView gallu;
	EdgeEffectViewPager viewPager;
	ImagePagerAdapter adapter;
	CirclePageIndicator mIndicator;
	FavouritesHandler treatHandler;
	ProgressDialog progress;
	/*
	 * public final String consumer_key = "gFUtrooktPrH1nkrvIzGQ"; public final
	 * String secret_key = "g36tP2wnGyDvcCqtI0AF2svwk7tYMd3tCf5LZdKOOw";
	 */
	private String string_msg = null;
	Vector<Restaurant> favourites;
	FavouritesHandler favHandler;
	boolean isPush = false;
	File casted_image;
	Restaurant restaurant;
	private GoogleMap googleMap;
	double lat = 0.0, lan = 0.0;
	Facebook mFacebook;
	SharedPreferences settings;
	AsyncFacebookRunner mAsyncFacebookRunner;
	private String fbId;
	Map<String, Integer> symbol_value = new HashMap<String, Integer>();
	Map<String, Integer> symbol_name_value = new HashMap<String, Integer>();
	int height, width;
	Bitmap marker_bitmap, cof_bitmap, nyanmeldt_bitmap;
	AdView adView;
	LinearLayout score_layout;
	TextView mad_txt, service_txt;
	public Vector<String> gettigList = new Vector<String>();
	// Url+ Bitmaps.
	public static HashMap<String, Bitmap> finalBitmaps;
	// To update images in a new thread.
	UpdateImages updateImages;
	Handler handler;
	Vector<String> urlList;
	Vector<ImageView> imgList;
	ImageView clasificationImage;
	TextView totalPoint;
	InitialInformation information;
	boolean isbought = false;
	String guideId;
	BasicInfoBean beanObj;
	PaymentInformation payInfo;
	String tagline = "", address = "", phone = "", website = "", body = "",
			cheapstPrice = "", opentime = "", zipcode = "",
			expensive_entre = "", cheapest_main_dish = "",
			expensive_main_dish = "", cheapest_dessert = "",
			expensive_dessert = "", cheapest_menu = "", expensive_menu = "",
			city = "", email = "", shareToFbTwitter = "", food = "",
			service = "";
	public static String Email_str = "Hele anmeldelsen kan du l�se i White Guide App.<br/> White Guide App giver dig fyldige beskrivelser og kritiske anmeldelser af 330 restauranter i hele Danmark. Du f�r desuden l�bende restaurantnyheder og anmeldelser af ny�bnede spisesteder. <br/><br />Hent din White Guide App fra App Store <a href=\"https://itunes.apple.com/us/app/white-guide-danmark/id928441209?ls=1&mt=8\">HER</a> <br />Hent din White Guide App fra Google Play <a href=\"https://play.google.com/store/apps/details?id=com.live.dk.whiteguide\">HER</a>";

	/***
	 * Runnable to download the images...update the adapter with each image
	 * download completion.
	 */
	class UpdateImages extends Thread {
		public int calculateInSampleSize(BitmapFactory.Options options,
				int reqWidth, int reqHeight) {
			// Raw height and width of image
			final int height = options.outHeight;
			final int width = options.outWidth;
			int inSampleSize = 1;

			if (height > reqHeight || width > reqWidth) {

				final int halfHeight = height / 2;
				final int halfWidth = width / 2;

				// Calculate the largest inSampleSize value that is a power of 2
				// and keeps both
				// height and width larger than the requested height and width.
				while ((halfHeight / inSampleSize) > reqHeight
						&& (halfWidth / inSampleSize) > reqWidth) {
					inSampleSize *= 2;
				}
			}

			return inSampleSize;
		}

		URL url;
		HttpURLConnection connection;
		InputStream input;
		Bitmap srcBmp;
		BitmapFactory.Options o;

		@Override
		public void run() {
			Runnable runnable = new Runnable() {

				@Override
				public void run() {
					try {
						for (String treat : urlList) {
							boolean isThere = false;
							try {
								if (null != finalBitmaps) {
									if (null != finalBitmaps.get(treat)) {
										isThere = true;
									}
								}
								if (!isThere) {
									url = new URL(treat);
									connection = (HttpURLConnection) url
											.openConnection();
									connection.setDoInput(true);
									connection.connect();
									input = connection.getInputStream();
									o = new BitmapFactory.Options();
									o.inJustDecodeBounds = false;
									o.inSampleSize = 2;
									srcBmp = BitmapFactory.decodeStream(input,
											null, o);
									finalBitmaps.put(treat, srcBmp);
								}
							} catch (Exception e) {
								finalBitmaps.put(treat, null);
							}
							handler.post(new Runnable() {

								@Override
								public void run() {
									if (null != adapter) {
										adapter.notifyDataSetChanged();
									}
								}
							});
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			new Thread(runnable).start();
		};
	}

	public class FacebookAuthListener implements AuthListener {

		public void onAuthSucceed() {
			SessionStore.save(mFacebook, getApplicationContext());
		}

		public void onAuthFail(String error) {
		}
	}

	class FacebookLogoutListener implements LogoutListener {
		public void onLogoutBegin() {
		}

		public void onLogoutFinish() {
		}
	}

	private class UserDetailsRequestListener implements DialogListener,
			RequestListener {

		@Override
		public void onComplete(String response, Object state) {
			try {
				JSONObject json = Util.parseJson(response);
				String friedsresponse = new FBConnectQueryProcessor()
						.requestFriendsList(getApplicationContext(), mFacebook);
				JSONObject mainObj = new JSONObject(friedsresponse);
				fbId = json.getString("id");
				if (mainObj.has("error")) {
					return;
				} else {
					doFbShare();
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FacebookError e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		@Override
		public void onIOException(IOException e, Object state) {

		}

		@Override
		public void onFileNotFoundException(FileNotFoundException e,
				Object state) {

		}

		@Override
		public void onMalformedURLException(MalformedURLException e,
				Object state) {

		}

		@Override
		public void onFacebookError(FacebookError e, Object state) {

		}

		@Override
		public void onComplete(Bundle values) {

		}

		@Override
		public void onFacebookError(FacebookError e) {

		}

		@Override
		public void onError(DialogError e) {

		}

		@Override
		public void onCancel() {

		}

	}

	class FacebookLoginDialogListener implements DialogListener {
		public void onComplete(Bundle values) {
			SessionEvents.onLoginSuccess();
			mAsyncFacebookRunner
					.request("me", new UserDetailsRequestListener());
			SessionStore.save(mFacebook, getApplicationContext());
			doFbShare();
		}

		public void onFacebookError(FacebookError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onError(DialogError error) {
			SessionEvents.onLoginError(error.getMessage());
		}

		public void onCancel() {
			SessionEvents.onLoginError("Action Canceled");
		}
	}

	/**
	 * Sharing on facebook
	 */
	private void doFbShare() {
		mFacebook = new Facebook(DataEngine.FB_APPID);// new
		// Facebook("193498163996120");
		mAsyncFacebookRunner = new AsyncFacebookRunner(mFacebook);
		SessionStore.restore(mFacebook, getApplicationContext());
		SessionEvents.addAuthListener(new FacebookAuthListener());
		SessionEvents.addLogoutListener(new FacebookLogoutListener());
		if (mFacebook.isSessionValid()) {

			Bundle params = new Bundle();
			params.putString("message", restaurant.getTitle().replace("#", ""));
			params.putString("link", shareToFbTwitter);
			// params.putString("caption", "{*actor*} just posted this!");

			String tempData = "";
			try {
				if (body.length() > 0) {
					tempData = body.substring(0, 250) + "...";
				}
			} catch (Exception e) {
				tempData = body;
			}
			params.putString("description", tempData);
			params.putString("name", restaurant.getTitle().replace("#", ""));
			params.putString(
					"picture",
					"http://www.whiteguide.se/sites/default/themes/wgtheme/gfx/static-logo-large.png");
			params.putString("to", fbId);
			mFacebook.dialog(this, "feed", params, new DialogListener() {

				@Override
				public void onFacebookError(FacebookError ee) {
				}

				@Override
				public void onError(DialogError ee) {
				}

				@Override
				public void onComplete(Bundle values) {
					if (!values.isEmpty())
						showFbDeliverySuccess();
				}

				@Override
				public void onCancel() {
				}
			});

		} else {
			mFacebook.authorize(DetailedRestaurantview.this,
					Util.FACEBOOK_PERMISSIONS, Facebook.FORCE_DIALOG_AUTH,
					new FacebookLoginDialogListener());
		}

	}

	/**
	 * Facebook success message
	 */
	private void showFbDeliverySuccess() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.facebook_success));
		builder.setTitle(getString(R.string.success));
		builder.setNegativeButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						try {
							if (progress.isShowing())
								progress.dismiss();
						} catch (Exception e) {
						}
					}
				});

		builder.show();

	}

	/**
	 * Sharing the data through the twitter.
	 */
	public void doTweetShare() {
		Twitt_Sharing twitt = new Twitt_Sharing(DetailedRestaurantview.this,
				DataEngine.consumer_key, DataEngine.secret_key);
		string_msg = restaurant.getTitle().replace("#", "") + "\n"
				+ shareToFbTwitter;
		String_to_File();
		twitt.shareToTwitter(string_msg, casted_image);

	}

	/**
	 * shows the dialog with facebook ,twitter and mail.
	 */
	public void doShare() {

		final Dialog view = new Dialog(this, android.R.style.Theme);
		// view.requestWindowFeature(Window.FEATURE_NO_TITLE);
		view.setContentView(R.layout.mail_twitter_facebook);
		view.getWindow().setBackgroundDrawable(
				new ColorDrawable(android.graphics.Color.TRANSPARENT));
		view.findViewById(R.id.shar_lay).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						view.dismiss();
					}
				});
		view.findViewById(R.id.twitter).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						doTweetShare();
						view.dismiss();
					}
				});
		view.findViewById(R.id.facebook).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						doFbShare();
						view.dismiss();

					}
				});
		/*
		 * view.findViewById(R.id.close_button).setOnClickListener(new
		 * OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { view.dismiss(); } });
		 */
		/**
		 * Mail listener,sends mail
		 */
		view.findViewById(R.id.emailTxt).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						String tempData = "";
						try {
							if (body.length() > 0) {
								tempData = body.substring(0, 250);
							}
						} catch (Exception e) {
							tempData = body;
						}
						string_msg = restaurant.getTitle().replace("#", "")
								+ "\n\n" + "<br>"+ address + "\n\n"+ "<br><br>" + tempData+"..." + "\n\n"+ "<br>"
								+ shareToFbTwitter + "<br><br>" + Email_str;
						Intent intent = new Intent(Intent.ACTION_SEND);
						intent.setType("plain/text");
						intent.putExtra(Intent.EXTRA_SUBJECT,
								getString(R.string.email_header)
										+ " "
										+ restaurant.getTitle()
												.replace("#", ""));
						intent.putExtra(Intent.EXTRA_TEXT,
								Html.fromHtml(string_msg));
						startActivity(Intent.createChooser(intent, ""));
						view.dismiss();

					}
				});
		view.show();/*
					 * final Dialog view = new Dialog(this);
					 * view.requestWindowFeature(Window.FEATURE_NO_TITLE);
					 * view.setContentView(R.layout.mail_twitter_facebook);
					 * view.findViewById(R.id.twitter).setOnClickListener( new
					 * OnClickListener() {
					 * 
					 * @Override public void onClick(View v) { doTweetShare();
					 * view.dismiss(); } });
					 * view.findViewById(R.id.facebook).setOnClickListener( new
					 * OnClickListener() {
					 * 
					 * @Override public void onClick(View v) { doFbShare();
					 * view.dismiss();
					 * 
					 * } });
					 * view.findViewById(R.id.emailTxt).setOnClickListener( new
					 * OnClickListener() {
					 * 
					 * @Override public void onClick(View v) { String tempData =
					 * ""; try { if (body.length() > 0) { tempData =
					 * body.substring(0, 250); } } catch (Exception e) {
					 * tempData = body; } string_msg =
					 * restaurant.getTitle().replace("#", "") + "\n\n" + address
					 * + "\n\n" + tempData + "\n\n" + shareToFbTwitter; Intent
					 * intent = new Intent(Intent.ACTION_SEND);
					 * intent.setType("plain/text");
					 * intent.putExtra(Intent.EXTRA_SUBJECT, restaurant
					 * .getTitle().replace("#", ""));
					 * intent.putExtra(Intent.EXTRA_TEXT, string_msg);
					 * startActivity(Intent.createChooser(intent, ""));
					 * view.dismiss();
					 * 
					 * } }); view.show();
					 */
	}

	TextView header_text;

	public class SymbolsAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return gettigList.size();
		}

		@Override
		public Object getItem(int position) {
			return position;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressWarnings("static-access")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ImageView image = new ImageView(DetailedRestaurantview.this);
			if (metrics.densityDpi == metrics.DENSITY_HIGH
					|| metrics.densityDpi == metrics.DENSITY_MEDIUM
					|| metrics.densityDpi == metrics.DENSITY_LOW) {
				image.setLayoutParams(new GridView.LayoutParams(50, 50));

			} else {
				if (metrics.densityDpi == metrics.DENSITY_XXHIGH) {
					image.setLayoutParams(new GridView.LayoutParams(120, 120));
				} else {
					image.setLayoutParams(new GridView.LayoutParams(80, 80));
				}
			}
			image.setScaleType(ScaleType.CENTER_CROP);
			try {
				image.setImageResource(symbol_value.get(gettigList
						.get(position)));
			} catch (Exception e) {
				image.setImageResource(R.drawable.appicon);
			}

			return image;
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		init();
		imgList = new Vector<ImageView>();
		payInfo = new PaymentInformation(getApplicationContext());
		isPush = getIntent().getBooleanExtra("PUSH", false);
		Log.i("ACTIVITY", "On Create of the ");
		beanObj = (BasicInfoBean) getIntent().getSerializableExtra("GUIDE");
		setContentView(R.layout.main);
		grid = (GridView) findViewById(R.id.symbols);
		// typeFace = Typeface.createFromAsset(getAssets(),
		// "HelveticaNeue.ttf");
		favourites = new Vector<Restaurant>();
		favHandler = new FavouritesHandler(getApplicationContext());
		favourites = favHandler.readTreats();
		imagecount = (TextView) findViewById(R.id.imagecount);
		score_layout = (LinearLayout) findViewById(R.id.scores_layout);
		mad_txt = (TextView) findViewById(R.id.mad);
		service_txt = (TextView) findViewById(R.id.service);
		restaurant = (Restaurant) getIntent().getSerializableExtra("RES");
		header_text = (TextView) findViewById(R.id.header_text);
		String headerText = restaurant.getTitle().replace("#", "");
		header_text.setText(headerText);
		adView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		if (DataEngine.showAds)
			adView.loadAd(adRequest);
		else
			adView.setVisibility(View.GONE);

		// Display Metrics object to get the screen Resolution.
		metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);

		// Height & width of the Screen.
		height = metrics.heightPixels;
		width = metrics.widthPixels;

		handler = new Handler();
		finalBitmaps = new HashMap<String, Bitmap>();
		urlList = new Vector<String>();

		clasificationImage = (ImageView) findViewById(R.id.classificationImg);

		totalPoint = (TextView) findViewById(R.id.count);
		openingtime = (TextView) findViewById(R.id.time);
		// openingtime.setTypeface(typeFace);
		price = (TextView) findViewById(R.id.price);
		phone_id = (TextView) findViewById(R.id.phone);
		address_id = (TextView) findViewById(R.id.address);
		mail_id = (TextView) findViewById(R.id.mail);
		descriptionHeading = (TextView) findViewById(R.id.textView1);
		detaildescription = (TextView) findViewById(R.id.textView2);
		descriptionfulltext = (TextView) findViewById(R.id.fulltext);
		viewPager = (EdgeEffectViewPager) findViewById(R.id.view_pager);
		viewPager.setEdgeEffectColor(Color.parseColor("#71D3FF"));
		mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
		treatHandler = new FavouritesHandler(this);
		FavBtnClicked = (ImageView) findViewById(R.id.FavBtn);
		gallery = (Button) findViewById(R.id.gallery);
		twitter = (ImageView) findViewById(R.id.button2);
		gallu = (GridView) findViewById(R.id.gallery1);
		scrolldetail = (EdgeEffectScrollView) findViewById(R.id.scrollView);
		scrolldetail.setEdgeEffectColor(Color.parseColor("#71D3FF"));
		progress = new ProgressDialog(this);
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));

		if (!NetworkChecker.isNetworkOnline(this)) {
			showErrorDialog();
			return;
		}
		Background sc = new Background();
		sc.execute();

		setListeners();

		lat = restaurant.getLat();
		lan = restaurant.getLon();

		initilizeMap();

	}

	/**
	 * Sets the values of symbols to hashmap
	 */
	private void init() {
		symbol_value.put("1", R.drawable.handikappvanligt_1);
		symbol_value.put("2", R.drawable.bar_2);
		symbol_value.put("3", R.drawable.enklare_mat_3);
		symbol_value.put("4", R.drawable.brunch_4);
		symbol_value.put("5", R.drawable.parkering_5);
		symbol_value.put("6", R.drawable.vegetariskt_6);
		symbol_value.put("7", R.drawable.overnattningsmojligheter_7);
		symbol_value.put("8", R.drawable.festvaning_8);
		symbol_value.put("9", R.drawable.chambre_separee_9);
		symbol_value.put("10", R.drawable.uteservering_10);
		symbol_value.put("14", R.drawable.horesta45);
		symbol_value.put("16", R.drawable.antal_platser);
	}

	/**
	 * Network alert
	 */
	private void showErrorDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.internet_error));
		builder.setTitle(getString(R.string.internet_errtitle));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
					}
				});
		builder.show();
	}

	/**
	 * Sets the listeners to this class elements
	 */
	private void setListeners() {
		descriptionfulltext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				showAlertBox();
			}
		});

		grid.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				// TODO Auto-generated method stub

				Intent nextActivity = new Intent(DetailedRestaurantview.this,
						SwipeView.class);
				startActivity(nextActivity);

			}
		});
		FavBtnClicked.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				boolean isFavourite = false;
				try {
					favourites = favHandler.readTreats();
					for (Restaurant dummy : favourites) {
						if (restaurant.getId().equalsIgnoreCase(dummy.getId())) {
							isFavourite = true;
							break;
						}
					}

				} catch (Exception e) {

				}
				if (isFavourite) {
					((ImageView) findViewById(R.id.FavBtn))
							.setImageResource(R.drawable.fav_open);
					treatHandler.deleteTreat(restaurant.getId());
				} else {
					((ImageView) findViewById(R.id.FavBtn))
							.setImageResource(R.drawable.fav_selected);
					treatHandler.writeTreat(restaurant);
				}
			}
		});

		descriptionfulltext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (beanObj.isGuideisBought()) {
					// detaildescription.setMaxLines(5000);
					descriptionfulltext.setVisibility(View.GONE);
				} else {
					showAlertBox();
				}
			}
		});
		findViewById(R.id.classificationImg).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (beanObj.isGuideisBought()
								|| beanObj
										.getGuideName()
										.equalsIgnoreCase(
												DetailedRestaurantview.this
														.getString(R.string.nyanmeldt_name))) {
							Intent nextActivity = new Intent(
									DetailedRestaurantview.this,
									CategoryDetails.class);
							startActivity(nextActivity);
						} else {
							showAlertBox();
						}
					}
				});
		gallery.setOnClickListener(new OnClickListener() {

			public void onClick(View arg0) {
				Intent nextActivity = new Intent(DetailedRestaurantview.this,
						GalleryPage.class);
				nextActivity.putExtra("IMAGES", urlList);
				nextActivity.putExtra("POSITION", 0);
				startActivity(nextActivity);
			}
		});

		twitter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				doShare();
			}
		});
	}

	/**
	 * Alerts the user to buy the the guide if it is not brought.
	 */
	protected void showAlertBox() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(getString(R.string.guide_buytxt_bef) + " "
				+ beanObj.getGuidePrice() + " "
				+ getString(R.string.guidetxt_after));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
						Home.setTab(4);
					}
				});
		builder.setNegativeButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
		builder.show();

	}

	/**
	 * Checks the respective user has brought the guides or not and displays the
	 * data accordingly.
	 * 
	 * @param purchased
	 */
	public void decideDataVisibility(boolean purchased) {
		try {
			boolean isFavourite = false;
			for (Restaurant dummy : favourites) {
				if (restaurant.getId().equalsIgnoreCase(dummy.getId())) {
					isFavourite = true;
					break;
				}
			}
			if (isFavourite)
				((ImageView) findViewById(R.id.FavBtn))
						.setImageResource(R.drawable.fav_selected);
			else
				((ImageView) findViewById(R.id.FavBtn))
						.setImageResource(R.drawable.fav_open);
		} catch (Exception e) {

		}
		if (purchased
				|| beanObj.getGuideName().equalsIgnoreCase(
						DetailedRestaurantview.this
								.getString(R.string.nyanmeldt_name))) {

			detaildescription.setText(body);
			descriptionfulltext.setVisibility(View.GONE);
			if (restaurant.getRestaurantClass().contains("1"))
				clasificationImage.setImageResource(R.drawable.intklass);
			else if (restaurant.getRestaurantClass().contains("2"))
				clasificationImage.setImageResource(R.drawable.masterklass);
			else if (restaurant.getRestaurantClass().contains("3"))
				clasificationImage.setImageResource(R.drawable.myketgodklass);
			else if (restaurant.getRestaurantClass().contains("4"))
				clasificationImage.setImageResource(R.drawable.godklass);
			else if (restaurant.getRestaurantClass().contains("7"))
				clasificationImage.setImageResource(R.drawable.brastalle);
			if (restaurant.getScore() > 0
					&& !restaurant.getRestaurantClass().contains("7")) {
				totalPoint.setVisibility(View.VISIBLE);
				totalPoint.setText("" + restaurant.getScore());
			}
		} else {
			String tempData = "";
			try {
				if (body.length() > 0) {
					tempData = body.substring(0, 250) + "...";
				}
			} catch (Exception e) {
				tempData = body;
			}
			detaildescription.setText(tempData);
			descriptionfulltext.setVisibility(View.VISIBLE);
		}
	}

	/**
	 * calls the respective class
	 * 
	 * @param v
	 */
	public void callswipe(View v) {
		Intent nextActivity = new Intent(this, SwipeView.class);
		nextActivity.putExtra("RES", restaurant);
		ListGroup parentActivity = (ListGroup) getParent();
		parentActivity.startChildActivity("swipe view", nextActivity);

	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	@SuppressLint("NewApi")
	private void initilizeMap() {
		if (googleMap == null) {
			// GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
		}
		// check if map is created successfully or not
		if (googleMap == null) {
			Toast.makeText(this, "Sorry! unable to create maps",
					Toast.LENGTH_SHORT).show();
		} else {
			googleMap.setOnMapClickListener(new OnMapClickListener() {

				@Override
				public void onMapClick(LatLng arg0) {
					// TODO Auto-generated method stub
					Intent intent = new Intent(DetailedRestaurantview.this,
							DetailedMap.class);
					intent.putExtra("RES", restaurant);
					startActivity(intent);
				}
			});
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(false);

			// Enable / Disable my location button
			// googleMap.getUiSettings().setMyLocationButtonEnabled(true);

			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);

			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);

			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);
			MarkerOptions marker;

			Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
					R.drawable.marker);
			Bitmap coffee_bitmap = BitmapFactory.decodeResource(getResources(),
					R.drawable.marker2);
			Bitmap Nytest_bit = BitmapFactory.decodeResource(getResources(),
					R.drawable.newres_marker);
			marker_bitmap = Bitmap.createScaledBitmap(bitmap, 57, 70, true);
			cof_bitmap = Bitmap.createScaledBitmap(coffee_bitmap, 57, 70, true);
			nyanmeldt_bitmap = Bitmap.createScaledBitmap(Nytest_bit, 57, 70,
					true);
			// Adding a marker
			if (beanObj.getGuideId().matches("58")) {
				marker = new MarkerOptions()
						.position(new LatLng(lat, lan))
						.icon(BitmapDescriptorFactory.fromBitmap(marker_bitmap))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			} else if (beanObj.getGuideId().matches("60")) {
				marker = new MarkerOptions().position(new LatLng(lat, lan))
						.icon(BitmapDescriptorFactory.fromBitmap(cof_bitmap))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			} else if (beanObj.getGuideId().matches("90")) {
				marker = new MarkerOptions()
						.position(new LatLng(lat, lan))
						.icon(BitmapDescriptorFactory
								.fromBitmap(nyanmeldt_bitmap))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			} else if (beanObj.getGuideId().matches("59")) {
				marker = new MarkerOptions()
						.position(new LatLng(lat, lan))
						.icon(BitmapDescriptorFactory
								.fromResource(R.drawable.marker))
						.title(restaurant.getTitle().replace("#", ""));
				googleMap.addMarker(marker);
			}
			LatLng latLng = new LatLng(lat, lan);
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
					latLng, 15);
			googleMap.animateCamera(cameraUpdate);

			/*
			 * if (width > 240 && width <= 320) {
			 * //splashImage.setImageResource(R.drawable.splash_320);
			 * marker_bitmap=Bitmap.createScaledBitmap(bitmap, 57, 70, true); }
			 * else if (width > 320 && width <= 480) {
			 * //splashImage.setImageResource(R.drawable.splash_480);
			 * marker_bitmap=Bitmap.createScaledBitmap(bitmap, 57, 70, true); }
			 * else if (width > 480 && width <= 640) {
			 * //splashImage.setImageResource(R.drawable.splash_640);
			 * marker_bitmap=Bitmap.createScaledBitmap(bitmap, 57, 70, true); }
			 * else if (width > 640 && width <= 800) {
			 * //splashImage.setImageResource(R.drawable.splash_1200);
			 * marker_bitmap=Bitmap.createScaledBitmap(bitmap, 89, 110, true); }
			 * else { //splashImage.setImageResource(R.drawable.splash_1200);
			 * marker_bitmap=Bitmap.createScaledBitmap(bitmap, 89, 110, true); }
			 * 
			 * // Adding a marker MarkerOptions marker = new MarkerOptions()
			 * .position(new LatLng(lat, lan)) //.snippet(restaurant.getCity())
			 * .icon(BitmapDescriptorFactory.fromBitmap(marker_bitmap) )
			 * .title(restaurant.getTitle().replace("#", ""));
			 * 
			 * googleMap.addMarker(marker); LatLng latLng = new LatLng(lat,
			 * lan); CameraUpdate cameraUpdate =
			 * CameraUpdateFactory.newLatLngZoom( latLng, 15);
			 * googleMap.animateCamera(cameraUpdate);
			 */

		}

	}

	/**
	 * finishes the activity when called.
	 * 
	 * @param v
	 */
	public void calling(View v) {

		if (isPush) {

			Intent notificationIntent = new Intent(this, Home.class);
			// set intent so it does not start a new activity
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
			notificationIntent.putExtras(getIntent().getExtras());
			notificationIntent.putExtra("PUSH_NOTIFICATION", false);
			startActivity(notificationIntent);
		}
		finish();
	}

	/**
	 * Adapter class for view pager
	 */
	private class ImagePagerAdapter extends PagerAdapter {

		@Override
		public int getCount() {
			if (urlList.size() > 0)
				return urlList.size();
			else
				return 1;
		}

		@Override
		public void notifyDataSetChanged() {
			for (int i = 0; i < imgList.size(); i++) {
				if (urlList.size() > 0 && urlList.size() > i) {

					if (null != finalBitmaps.get(urlList.get(i))) {
						imgList.get(i).setImageBitmap(
								finalBitmaps.get(urlList.get(i)));
					} else
						imgList.get(i).setImageResource(R.drawable.wg_noimage);
				} else {
					imgList.get(i).setImageResource(R.drawable.wg_noimage);
				}
			}
			super.notifyDataSetChanged();

		}

		@Override
		public boolean isViewFromObject(View view, Object object) {
			return view == ((ImageView) object);
		}

		@SuppressWarnings("deprecation")
		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			Context context = DetailedRestaurantview.this;
			imageView = new ImageView(context);
			imageView.setLayoutParams(new LayoutParams(
					LayoutParams.FILL_PARENT, LayoutParams.MATCH_PARENT));

			// imageView.setCropToPadding(true);
			imageView.setScaleType(ScaleType.CENTER_CROP);
			if (urlList.size() > 0) {
				if (null != finalBitmaps.get(urlList.get(position)))
					imageView.setImageBitmap(finalBitmaps.get(urlList
							.get(position)));
				else
					imageView.setImageResource(R.drawable.wg_noimage);
				imagecount.setText("" + urlList.size());
			} else {
				imageView.setImageResource(R.drawable.wg_noimage);
			}
			imageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if (urlList.size() > 0) {
						Intent nextActivity = new Intent(
								DetailedRestaurantview.this, GalleryPage.class);
						nextActivity.putExtra("IMAGES", urlList);
						nextActivity.putExtra("RES", restaurant);
						nextActivity.putExtra("POSITION", position);
						startActivity(nextActivity);
					}
				}
			});
			imgList.add(imageView);
			((ViewPager) container).addView(imageView);
			return imageView;
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			((ViewPager) container).removeView((ImageView) object);
		}
	}

	@Override
	public void onBackPressed() {
		// DetailedRestaurantview.this.overridePendingTransition(R.anim.slide_out_left,
		// R.anim.slide_in_right);
		if (isPush) {

			Intent notificationIntent = new Intent(this, Home.class);
			// set intent so it does not start a new activity
			notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_SINGLE_TOP);
			notificationIntent.putExtras(getIntent().getExtras());
			notificationIntent.putExtra("PUSH_NOTIFICATION", false);
			startActivity(notificationIntent);
		}
		finish();
	}

	/**
	 * Does the file creation with respective fields for twitter share
	 * 
	 * @return file to share in twitter
	 */
	public File String_to_File() {

		try {
			File rootSdDirectory = Environment.getExternalStorageDirectory();

			casted_image = new File(rootSdDirectory, "attachment.jpg");
			if (casted_image.exists()) {
				casted_image.delete();
			}
			casted_image.createNewFile();

			FileOutputStream fos = new FileOutputStream(casted_image);
			try {

				BitmapFactory.Options options = new BitmapFactory.Options();
				options.inSampleSize = 2;
				BufferedOutputStream bos = new BufferedOutputStream(fos);
				Bitmap mBitmap = BitmapFactory.decodeResource(getResources(),
						R.drawable.appicon);
				mBitmap.compress(CompressFormat.JPEG, 100, bos);

				bos.flush();

				bos.close();

			} catch (Exception e) {
				return null;
			}

			return casted_image;

		} catch (Exception e) {
		}
		return casted_image;

	}

	String response1 = "";

	public class Background extends AsyncTask<String, Void, String> {

		@Override
		protected void onPreExecute() {
			if (null != progress) {
				if (!progress.isShowing())
					progress.show();
			}
		}

		@Override
		protected void onPostExecute(String result) {
			JSONObject jobjdata = null;
			if (finalBitmaps.size() >= 0) {
				adapter = new ImagePagerAdapter();
				viewPager.setAdapter(adapter);
				mIndicator.setViewPager(viewPager);
				updateImages = new UpdateImages();
				updateImages.start();
			} else {
				gallery.setVisibility(View.GONE);
				imagecount.setVisibility(View.GONE);
				mIndicator.setVisibility(View.GONE);
			}
			if (finalBitmaps.size() <= 4) {
				gallery.setVisibility(View.GONE);
				imagecount.setVisibility(View.GONE);
			}
			if (urlList.size() <= 1) {
				mIndicator.setVisibility(View.INVISIBLE);
			}
			try {
				jobjdata = new JSONObject(result);
				JSONArray jarray = jobjdata.getJSONArray("symbols");

				for (int i = 0; i < jarray.length(); i++) {
					JSONObject info = jarray.getJSONObject(i);
					String id = info.getString("id");
					if (symbol_value.containsKey(id))
						gettigList.add(id);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				tagline = jobjdata.optString("tagline");
				address = jobjdata.optString("address");
				email = jobjdata.optString("email");
				phone = jobjdata.optString("phone");
				website = jobjdata.optString("website");
				body = jobjdata.optString("body");
			} catch (Exception e) {
			}
			try {
				cheapstPrice = jobjdata.optString("cheapest_entre");
				expensive_entre = jobjdata.optString("expensive_entre");
				cheapest_main_dish = jobjdata.optString("cheapest_main_dish");
				expensive_main_dish = jobjdata.optString("expensive_main_dish");
				cheapest_dessert = jobjdata.optString("cheapest_dessert");
				expensive_dessert = jobjdata.optString("expensive_dessert");
				cheapest_menu = jobjdata.optString("cheapest_menu");
				expensive_menu = jobjdata.optString("expensive_menu");
				opentime = jobjdata.optString("open");
				zipcode = jobjdata.optString("zip");
				city = jobjdata.optString("city");
				shareToFbTwitter = jobjdata.optString("wgurl");
				service = jobjdata.getJSONObject("scores").optString("service");
				food = jobjdata.getJSONObject("scores").optString("food");

			} catch (Exception e) {
			}
			try {
				StringBuilder builder = new StringBuilder();
				if (null != cheapstPrice && "null" != cheapstPrice
						&& !"".equalsIgnoreCase(cheapstPrice)
						&& null != expensive_entre && "null" != expensive_entre
						&& !"".equalsIgnoreCase(expensive_entre)) {
					builder.append("Forret " + cheapstPrice + "-"
							+ expensive_entre + " kr.,");
				}
				if (null != cheapest_main_dish && "null" != cheapest_main_dish
						&& !"".equalsIgnoreCase(cheapest_main_dish)
						&& null != expensive_main_dish
						&& "null" != expensive_main_dish
						&& !"".equalsIgnoreCase(expensive_main_dish)) {
					builder.append(" hovedret " + cheapest_main_dish + "-"
							+ expensive_main_dish + " kr.,");

				}
				if (null != cheapest_dessert && "null" != cheapest_dessert
						&& !"".equalsIgnoreCase(cheapest_dessert)
						&& null != expensive_dessert
						&& "null" != expensive_dessert
						&& !"".equalsIgnoreCase(expensive_dessert)) {
					builder.append(" dessert " + cheapest_dessert + "-"
							+ expensive_dessert + " kr.,");

				}
				if (null != cheapest_menu && "null" != cheapest_menu
						&& !"".equalsIgnoreCase(cheapest_menu)
						&& null != expensive_menu && "null" != expensive_menu
						&& !"".equalsIgnoreCase(expensive_menu)) {
					builder.append(" Menu " + cheapest_menu + "-"
							+ expensive_menu + " kr.");
				}
				if (beanObj.isGuideisBought() && restaurant.getScore() > 0) {
					if (restaurant.getRestaurantClass().contains("7")) {
						score_layout.setVisibility(View.GONE);
					} else {
						score_layout.setVisibility(View.VISIBLE);
						if (null != service && "null" != service
								&& !"".equalsIgnoreCase(service)) {
							mad_txt.setText("\nMad " + food + "/40");
						}
						// builder.append("\nMad: "+food+"/40");
						if (null != service && "null" != service
								&& !"".equalsIgnoreCase(service)) {
							service_txt.setText("\nService " + service + "/25");
						}
						// builder.append("\nService: "+service+"/25");
					}

				}
				String price_txt = "";
				try {
					price_txt = builder.toString();
					if (price_txt.endsWith(",")) {
						Log.i("TEST", "HERE IS" + price_txt);

						price_txt = price_txt.substring(0,
								price_txt.length() - 1);
					}

				} catch (Exception e) {
					Log.i("TEST", "NOT THERE" + e.getLocalizedMessage());
				}
				if (price_txt.equals("")) {
					price_txt = "Mangler.";
				}
				if (builder.length() == 0) {
					builder.append("Mangler.");
				}
				if (email.equals("null")) {
					email = "Mangler.";
				}
				if (address.equals("null")) {
					address = "Mangler.";
				}
				if (phone.equals("null")) {
					phone = "Mangler.";
				}
				if (website.equals("null")) {
					website = "Mangler.";
				}
				if (zipcode.equals("null")) {
					zipcode = "Mangler.";
				}
				if (city.equals("null")) {
					city = "Mangler.";
				}
				Log.i("TEST", "THE TEXT HERE IS " + price_txt);
				openingtime.setText(opentime);
				// openingtime.setTypeface(typeFace);
				price.setText(price_txt);
				// price.setTypeface(typeFace);
				address_id.setText(address + "," + "\n" + zipcode + " " + city);
				// address_id.setTypeface(typeFace);
				phone_id.setText(phone);
				// phone_id.setTypeface(typeFace);
				mail_id.setText(website);

				// mail_id.setTypeface(typeFace);
				descriptionHeading.setText(tagline);
				// descriptionHeading.setTypeface(typeFace);
				detaildescription.setText(body);
				// detaildescription.setTypeface(typeFace);
				// detaildescription.setEllipsize(TruncateAt.MIDDLE);
				SymbolsAdapter symbolsAdapter = new SymbolsAdapter();
				grid.setAdapter(symbolsAdapter);

				findViewById(R.id.scrollView).setVisibility(View.VISIBLE);
				findViewById(R.id.button2).setVisibility(View.VISIBLE);
				decideDataVisibility(beanObj.isGuideisBought()
						|| payInfo.isThisGuidebought(beanObj.getGuideId(),
								beanObj.getGuideYear()));

				phone_id.setLinkTextColor(Color.parseColor("#000000"));
				mail_id.setLinkTextColor(Color.parseColor("#000000"));
				address_id.setLinkTextColor(Color.parseColor("#000000"));

			} catch (Exception e) {
				e.printStackTrace();
			}
			if (null != progress) {
				if (progress.isShowing())
					progress.dismiss();
			}
		}

		@Override
		protected String doInBackground(String... params) {
			StringBuilder builder = new StringBuilder();
			HttpClient httpClient = new DefaultHttpClient();
			String parse_url = null;
			if (beanObj.getGuideName().equalsIgnoreCase(
					DetailedRestaurantview.this
							.getString(R.string.nyanmeldt_name))) {
				parse_url = DataEngine.BASE_URL + "getnytestatrestaurant/"
						+ restaurant.getId() + "?access=harmoniska";
			} else {
				parse_url = DataEngine.BASE_URL + "restaurants/"
						+ restaurant.getId() + "?access=harmoniska";
			}
			Log.e("DeTAIL", "----------parse_url-------------" + parse_url);
			HttpGet httpGet = new HttpGet(parse_url);
			try {
				HttpResponse response = httpClient.execute(httpGet);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == 200) {
					HttpEntity entity = response.getEntity();
					InputStream content = entity.getContent();
					BufferedReader reader = new BufferedReader(
							new InputStreamReader(content));
					String line;
					while ((line = reader.readLine()) != null) {
						builder.append(line);
					}
				} else {
				}
				response1 = builder.toString();
			} catch (Exception e) {
				e.printStackTrace();
			}
			JSONObject jobjdata = null;
			try {
				jobjdata = new JSONObject(response1);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			JSONArray picturearray = null;
			try {
				picturearray = jobjdata.getJSONArray("pictures");
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (null != picturearray)
				for (int k = 0; k < picturearray.length(); k++) {
					String picture = null;
					try {
						picture = picturearray.getString(k);
					} catch (JSONException e) {
						e.printStackTrace();
					}
					if (null != picture) {
						finalBitmaps.put(picture, null);
						urlList.add(picture);
					}
				}
			return response1;
		}

	}

	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			super.onBackPressed();
			return true;
		}
		return false;

	}
}