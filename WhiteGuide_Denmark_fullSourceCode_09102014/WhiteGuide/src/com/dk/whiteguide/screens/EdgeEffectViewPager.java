package com.dk.whiteguide.screens;

import com.live.dk.whiteguide.R;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;

public class EdgeEffectViewPager extends android.support.v4.view.ViewPager {


	public EdgeEffectViewPager(Context context) {
		this(context, null);
	}

	public EdgeEffectViewPager(Context context, AttributeSet attrs) {
		super(new ContextWrapperEdgeEffect(context), attrs);
    init(context, attrs, 0);
	}



  private void init(Context context, AttributeSet attrs, int defStyle){
    int color = context.getResources().getColor(R.color.default_edgeeffect_color);

    if (attrs != null) {
      TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.EdgeEffectView, defStyle, 0);
      color = a.getColor(R.styleable.EdgeEffectView_edgeeffect_color, color);
      a.recycle();
    }
    setEdgeEffectColor(color);
  }

  public void setEdgeEffectColor(int edgeEffectColor){
    ((ContextWrapperEdgeEffect)  getContext()).setEdgeEffectColor(edgeEffectColor);
  }
}
