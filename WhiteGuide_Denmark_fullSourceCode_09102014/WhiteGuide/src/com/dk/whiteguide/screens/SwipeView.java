package com.dk.whiteguide.screens;

import java.util.ArrayList;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.dk.whiteguide.helpers.CustomGridViewAdapter;
import com.dk.whiteguide.helpers.Item;
import com.live.dk.whiteguide.R;

/**
 * Represents the view of all the symbols explanation
 * @author Conevo
 */
public class SwipeView extends AnimActivity implements OnClickListener,
		OnItemClickListener {
	GridView gridView;
	ArrayList<Item> gridArray = new ArrayList<Item>();
	CustomGridViewAdapter customGridAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.swipe_activity);

		findViewById(R.id.back).setOnClickListener(this);
		Bitmap homeIcon = BitmapFactory.decodeResource(this.getResources(),R.drawable.antal_platser);
		Bitmap userIcon = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.uteservering_10);
		Bitmap homeIcon1 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.handikappvanligt_1);
		//Bitmap userIcon1 = BitmapFactory.decodeResource(this.getResources(),
			//	R.drawable.utmarkt_dryckesutbud_11);
		Bitmap homeIcon2 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.bar_2);
		//Bitmap userIcon2 = BitmapFactory.decodeResource(this.getResources(),
			//	R.drawable.hoga_eko_ambitioner_12);
		Bitmap homeIcon3 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.enklare_mat_3);
		//Bitmap userIcon3 = BitmapFactory.decodeResource(this.getResources(),
			//	R.drawable.hjartekrog_81);
		Bitmap homeIcon4 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.brunch_4);
		//Bitmap userIcon4 = BitmapFactory.decodeResource(this.getResources(),
			//	R.drawable.ny_82);
		Bitmap homeIcon5 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.parkering_5);
		Bitmap userIcon5 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.horesta90);
		Bitmap homeIcon6 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.vegetariskt_6);
		Bitmap userIcon6 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.overnattningsmojligheter_7);
		//Bitmap homeIcon7 = BitmapFactory.decodeResource(this.getResources(),
			//	R.drawable.american_express_15);
		Bitmap userIcon7 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.festvaning_8);
		Bitmap userIcon8 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.chambre_separee_9);
		//Bitmap wifi = BitmapFactory.decodeResource(this.getResources(),
			//	R.drawable.gratis_wifi_72);
		//Bitmap krav = BitmapFactory.decodeResource(this.getResources(),
			//	R.drawable.krav);
		Bitmap userIcon10 = BitmapFactory.decodeResource(this.getResources(),
				R.drawable.lunchsymbol_90);
		
		// � � �
		 gridArray.add(new Item(homeIcon, "Antal pladser"));
		gridArray.add(new Item(userIcon, "Udeservering"));
		gridArray.add(new Item(homeIcon1, "Handicapvenligt"));
		//gridArray.add(new Item(userIcon1,
			//	"Udm�rket og/eller specielt udbud af drikkevarer"));
		gridArray.add(new Item(homeIcon2, "Bar"));
		//gridArray.add(new Item(userIcon2, "H�je �kologiske ambitioner"));
		gridArray.add(new Item(homeIcon3,
				"Ogs� servering i baren"));
		//gridArray.add(new Item(userIcon3, "Hjertesund kro"));
		gridArray.add(new Item(homeIcon4, "Brunch"));
		//gridArray.add(new Item(userIcon4, "Ny restaurant i White Guide 2014"));
		gridArray.add(new Item(homeIcon5, "Parkering"));
		gridArray
				.add(new Item(userIcon5, "Medlem af Horesta"));
		gridArray.add(new Item(homeIcon6, "Vegetariske retter"));
		gridArray.add(new Item(userIcon6, "Overnatningsmu-ligheder"));
		//gridArray.add(new Item(homeIcon7, "Anvender American Express"));
		gridArray.add(new Item(userIcon7, "Festlokaler"));
		gridArray.add(new Item(userIcon8, "Chambre separ�e"));
		gridArray.add(new Item(userIcon10, "S�rligt fokus p� frokost"));
		//gridArray.add(new Item(wifi, "Gratis WiFi"));
		//gridArray.add(new Item(krav, "krav"));
		

		gridView = (GridView) findViewById(R.id.gridView1);
		customGridAdapter = new CustomGridViewAdapter(this, R.layout.row_grid,
				gridArray);
		gridView.setAdapter(customGridAdapter);
		
		//gridView.setOnItemClickListener(this);
	}

	@Override
	public void onClick(View v) {
		finish();
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		finish();
	}
}
