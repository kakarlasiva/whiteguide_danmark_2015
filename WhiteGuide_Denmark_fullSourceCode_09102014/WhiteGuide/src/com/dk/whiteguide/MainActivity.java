package com.dk.whiteguide;

import java.util.Vector;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.View;

import com.live.dk.whiteguide.R;
import com.dk.whiteguide.dataengine.DataEngine;
import com.dk.whiteguide.dataengine.DataHandler;
import com.dk.whiteguide.dataengine.FavouritesHandler;
import com.dk.whiteguide.dataengine.InitialInformation;
import com.dk.whiteguide.dataengine.PaymentInformation;
import com.dk.whiteguide.dataengine.Restaurant;
import com.dk.whiteguide.helpers.BasicInfoBean;
import com.dk.whiteguide.parsers.RestaurantsParser;
import com.dk.whiteguide.screens.DetailedRestaurantview;
import com.dk.whiteguide.tabgroup.Home;
import com.dk.whiteguide.tabgroup.MapGroup;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * Represents the Mapview for all the restaurants/Caf�er
 * @author Conevo
 */
public class MainActivity extends Activity implements LocationListener {
	public static final String TAG = "KARTA";
	private GoogleMap googleMap;
	AdView adView;
	RestaurantsParser parser;
	Vector<Double> lat, lon;
	Vector<Restaurant> restaurants;
	FavouritesHandler treatHandler;
	LocationManager locationManager;
	String provider;
	Location location;
	DataHandler dataHandler;
	InitialInformation information;
	PaymentInformation payInfo;
	ProgressDialog progress;
	int height,width;
	Bitmap marker_bitmap;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		lat = new Vector<Double>();
		lon = new Vector<Double>();
		progress = new ProgressDialog(this.getParent());
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		information = new InitialInformation(getApplicationContext());
		payInfo = new PaymentInformation(getApplicationContext());
		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		
		// Height & width of the Screen.
		height = displaymetrics.heightPixels;
		width = displaymetrics.widthPixels;
		restaurants = new Vector<Restaurant>();
		setContentView(R.layout.karta);
		adView = (AdView) findViewById(R.id.adView);
		String name = "Restaurang";
		try {
			name = new InitialInformation(this).readData().get(0)
					.getGuideName();
		} catch (Exception e) {
			name = "Restaurang";
		}
		dataHandler = new DataHandler(this.getParent(), name);
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		if (locationManager.getProviders(true).size() > 0) {
			for (int i = 0; i < locationManager.getAllProviders().size(); i++) {
				provider = locationManager.getAllProviders().get(i);
				location = locationManager.getLastKnownLocation(provider);
				if (null == location) {
					locationManager
							.requestLocationUpdates(provider, 0, 0, this);
				} else {
					break;
				}
			}
		} else {
			showAlert();
			return;
		}
		// Initialize the location fields
		if (location != null) {
			onLocationChanged(location);
		} else {
			showAlert();
		}
		new BackGround().execute();
	}

	/**
	 * Sets the slider view to be opened.
	 * @param v
	 */
	public void calling(View v) {
		Home.setDrawer();
	}

	/**
	 * Does the background for storing the values of lat and lon of all the restaurants and initializing the map.
	 */
	class BackGround extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			if (null != progress) {
				if (!progress.isShowing())
					progress.show();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			dotheRequired();
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			initilizeMap();
			if (null != progress) {
				if (progress.isShowing())
					progress.dismiss();
			}
		}
	}

	/**
	 * Shows alert when gps is not on in the device
	 */
	private void showAlert() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getParent());
		builder.setMessage(getString(R.string.location_request));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Intent myIntent = new Intent(
								android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
						startActivity(myIntent);
					}
				});
		builder.setNegativeButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
		builder.show();
	}

	/**
	 *  Does the storing the lat and lon of all restaurants to vectors.
	 */
	@SuppressWarnings("unused")
	public void dotheRequired() {
		// TODO Auto-generated method stub
		double lattitude, longitude;
		//location = null;
		if (location == null) {
			lattitude = 55.67;
			longitude = 12.56;
		} else {
			lattitude = location.getLatitude();
			longitude = location.getLongitude();
		}
		restaurants = new Vector<Restaurant>();
		lat = new Vector<Double>();
		lon = new Vector<Double>();
		String name = "Restaurang";
		try {
			name = new InitialInformation(this).readData().get(0)
					.getGuideName();
		} catch (Exception e) {
			e.printStackTrace();
		}
		int count = 0;
		try {
			count = information.readData().size();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (count > 0) {
			for (BasicInfoBean beanTemp : information.readData()) {
				dataHandler = new DataHandler(this.getParent(),
						beanTemp.getGuideName());
				for (Restaurant restaurant : dataHandler.readTreats()) {
					try {
							restaurants.add(restaurant);
							lat.add(restaurant.getLat());
							lon.add(restaurant.getLon());
						} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		} else {
			dataHandler = new DataHandler(this.getParent(), name);
			for (Restaurant restaurant : dataHandler.readTreats()) {
				try {
						restaurants.add(restaurant);
						lat.add(restaurant.getLat());
						lon.add(restaurant.getLon());
					} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 *  Does calculation for the nearest restaurants from the current location
	 * @param d  current lat
	 * @param e  current lon
	 * @param f  restaurant lat
	 * @param g  restaurant lon
	 * @return
	 */
	public double distFrom(double d, double e, double f, double g) {
		double earthRadius = 3958.75;
		double dLat = Math.toRadians(f - d);
		double dLng = Math.toRadians(g - e);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
				+ Math.cos(Math.toRadians(d)) * Math.cos(Math.toRadians(f))
				* Math.sin(dLng / 2) * Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double dist = earthRadius * c;

		int meterConversion = 1609;
		return (double) (dist * meterConversion);
	}

	/**
	 * function to load map If map is not created it will create it for you
	 * */
	@SuppressLint("NewApi")
	private void initilizeMap() {
		if (googleMap == null) { 
			AdRequest adRequest = new AdRequest.Builder().build();
			// Start loading the ad in the background.
			if (DataEngine.showAds)	adView.loadAd(adRequest);
			else adView.setVisibility(View.GONE);
			try
			{
			googleMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
		// check if map is created successfully or not
		if (googleMap == null) {

		} else {
			googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
			// Showing / hiding your current location
			if (null != location) {
				googleMap.setMyLocationEnabled(true);
			}

			// Enable / Disable zooming controls
			googleMap.getUiSettings().setZoomControlsEnabled(false);

			// Enable / Disable my location button
			googleMap.getUiSettings().setMyLocationButtonEnabled(true);

			// Enable / Disable Compass icon
			googleMap.getUiSettings().setCompassEnabled(true);

			// Enable / Disable Rotate gesture
			googleMap.getUiSettings().setRotateGesturesEnabled(true);

			// Enable / Disable zooming functionality
			googleMap.getUiSettings().setZoomGesturesEnabled(true);
			// lets place some 10 random markers
			 Bitmap bitmap=BitmapFactory.decodeResource(getResources(),R.drawable.marker);
		        
			if (width > 240 && width <= 320) {
				//splashImage.setImageResource(R.drawable.splash_320);
				marker_bitmap=Bitmap.createScaledBitmap(bitmap, 57, 70, true);
			} else if (width > 320 && width <= 480) {
				//splashImage.setImageResource(R.drawable.splash_480);
				marker_bitmap=Bitmap.createScaledBitmap(bitmap, 57, 70, true);
			} else if (width > 480 && width <= 640) {
				//splashImage.setImageResource(R.drawable.splash_640);
				marker_bitmap=Bitmap.createScaledBitmap(bitmap, 57, 70, true);
			} else if (width > 640 && width <= 800) {
				//splashImage.setImageResource(R.drawable.splash_1200);
				marker_bitmap=Bitmap.createScaledBitmap(bitmap, 89, 110, true);
			} else {
				//splashImage.setImageResource(R.drawable.splash_1200);
				marker_bitmap=Bitmap.createScaledBitmap(bitmap, 89, 110, true);
			}
			 
			for (int i = 0; i < lat.size(); i++) {
				// Adding a marker
				try{
				MarkerOptions marker = new MarkerOptions()
						.position(new LatLng(lat.get(i), lon.get(i)))
						//.snippet(restaurants.get(i).getCity())
						.icon(BitmapDescriptorFactory.fromBitmap(marker_bitmap))
						.title(restaurants.get(i).getTitle().replace("#", ""));
				googleMap.addMarker(marker); 
				}catch(Exception e){
					e.printStackTrace();
				}
				googleMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

							@Override
							public void onInfoWindowClick(Marker arg0) {
								// TODO Auto-generated method stub
								boolean is_purchased = false;
								String rest_title = "";
								int position = lat.indexOf(arg0.getPosition().latitude);
								Restaurant tempObj = null;
								if (position >= 0) {
									try {
										tempObj = restaurants.get((lat
												.indexOf(arg0.getPosition().latitude)));
									} catch (Exception e) {

									}
								}
								if (null == tempObj) {
									try {
										for (Restaurant restObj : restaurants) {
											rest_title = restObj.getTitle()
													.replace("#", "");
											if (arg0.getTitle()
													.trim()
													.equalsIgnoreCase(
															rest_title)) {
												tempObj = restObj;
												break;
											}
										}
									} catch (Exception e) {

									}
								}
								if (null != tempObj) {
									BasicInfoBean dummy = null;
									for (BasicInfoBean beanobj : information
											.readData()) {
										if (tempObj.getGuideId()
												.equalsIgnoreCase(
														beanobj.getGuideId())
												&& tempObj
														.getGuideYear()
														.equalsIgnoreCase(
																beanobj.getGuideYear())) {
											if (beanobj.isGuideisBought()) {
												is_purchased = true;
											}
											dummy = beanobj;
											break;
										}
									}
									if (!is_purchased) {
										is_purchased = payInfo
												.isThisGuidebought(
														tempObj.getId(),
														tempObj.getGuideYear());
									}
									handleThisCall(dummy, tempObj);
								}
							}
						});
			}

			if (null != location) {
				LatLng latLng = new LatLng(location.getLatitude(),
						location.getLongitude());
				CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
						latLng, 15);
				googleMap.animateCamera(cameraUpdate);
			}

		}

	}

	/**
	 *  Does to a particular restaurant view when click on mapview
	 */
	private void handleThisCall(final BasicInfoBean dummy,
			final Restaurant tempObj) {
		Home.closeDrawer();

		new Handler().post(new Runnable() {

			@Override
			public void run() {
				Intent nextActivity = new Intent(MainActivity.this,
						DetailedRestaurantview.class);
				nextActivity.putExtra("RES", tempObj);
				nextActivity.putExtra("GUIDE", dummy);
				MapGroup parentActivity = (MapGroup) getParent();
				parentActivity.startActivity(nextActivity);
				//parentActivity.startChildActivity("NEW_INFO", nextActivity);
			}
		});

	}

	protected void showAlertBox() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getParent());
		builder.setMessage(getString(R.string.unlock_information));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Home.setTab(3);
					}
				});
		builder.setNegativeButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
		builder.show();

	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		this.location = location;
		if (null != location) {
			BackGround backGround = new BackGround();
			backGround.execute();
			try {
				locationManager.removeUpdates(this);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}
}
