package com.dk.whiteguide;

import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.dk.whiteguide.dataengine.DataEngine;
import com.dk.whiteguide.dataengine.DataHandler;
import com.dk.whiteguide.dataengine.PaymentInformation;
import com.dk.whiteguide.dataengine.Restaurant;
import com.dk.whiteguide.helpers.AZRestaurantsAdapter;
import com.dk.whiteguide.helpers.BasicInfoBean;
import com.dk.whiteguide.helpers.CategoryRestaurantAdapter;
import com.dk.whiteguide.helpers.CityWiseRestaurantsAdapter;
import com.dk.whiteguide.helpers.SideBar;
import com.dk.whiteguide.helpers.SoftKeyboardHandledLinearLayout;
import com.dk.whiteguide.helpers.SoftKeyboardHandledLinearLayout.SoftKeyboardVisibilityChangeListener;
import com.dk.whiteguide.parsers.RestaurantsParser;
import com.dk.whiteguide.screens.AnimActivity;
import com.dk.whiteguide.screens.DetailedRestaurantview;
import com.dk.whiteguide.screens.EdgeEffectListView;
import com.dk.whiteguide.screens.FilteredRestaurants;
import com.dk.whiteguide.tabgroup.Home;
import com.dk.whiteguide.tabgroup.ListGroup;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.live.dk.whiteguide.R;

/**
 * Represents all the restaurants to be shown in a list view with three buttons
 * as all restaurants,city wise and category wise. 
 * @author Conevo
 */
public class RestaurantsList extends AnimActivity {
	EdgeEffectListView list;
	EditText search;
	Button a_z, cities, categories;
	PaymentInformation info;
	SideBar sideBar;
	Vector<Restaurant> azRestaurants, cityRestaurants, categoryRestaurants,
			totalRestaurants;
	AZRestaurantsAdapter adapter;
	CityWiseRestaurantsAdapter cityAdapter;
	CategoryRestaurantAdapter catgAdapter;
	Vector<String> citiesList;
	BasicInfoBean beanObj;
	boolean iskeyboardvisible=false;
	AdView adView;
	boolean azSelected = false, citiesSelected = false,
			categorySelected = false;

	DataHandler dataHandler;
	private int brastt_count = 0, godclass_count = 0, interclass_count = 0,
			myketclass_count = 0, masterclas_count = 0;
	Background background;
	private ProgressDialog progress;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.contact_dropdown_list_item);
		SoftKeyboardHandledLinearLayout mainView = (SoftKeyboardHandledLinearLayout) findViewById(R.id.mainView);
		mainView.setOnSoftKeyboardVisibilityChangeListener(
				new SoftKeyboardVisibilityChangeListener() {
					@Override
					public void onSoftKeyboardShow() {
						// TODO: do something here
						sideBar.setVisibility(View.INVISIBLE);
						iskeyboardvisible=true;
					}
					
					@Override
					public void onSoftKeyboardHide() {
						// TODO: do something here
						sideBar.setVisibility(View.VISIBLE);
						iskeyboardvisible=false;
					}
				});
		beanObj = (BasicInfoBean) getIntent().getExtras().getSerializable(
				"INFO");
		info = new PaymentInformation(getApplicationContext());
		adView = (AdView) findViewById(R.id.adView);
		AdRequest adRequest = new AdRequest.Builder().build();
		if (DataEngine.showAds)
			adView.loadAd(adRequest);
		else
			adView.setVisibility(View.GONE);
		dataHandler = new DataHandler(this.getParent(), beanObj.getGuideName());

		progress = new ProgressDialog(this.getParent());
		progress.setCancelable(true);
		progress.setMessage(getString(R.string.loading));
		list = (EdgeEffectListView) findViewById(R.id.myListView);
		list.setEdgeEffectColor(Color.parseColor("#71D3FF"));
		search = (EditText) findViewById(R.id.searchContacts);
		a_z = (Button) findViewById(R.id.restaurants);
		cities = (Button) findViewById(R.id.cities);
		categories = (Button) findViewById(R.id.categories);
		totalRestaurants = new Vector<Restaurant>();
		azRestaurants = new Vector<Restaurant>();
		cityRestaurants = new Vector<Restaurant>();
		categoryRestaurants = new Vector<Restaurant>();
		citiesList = new Vector<String>();
		
		if (beanObj.getGuideId().equalsIgnoreCase("58")) {
			search.setHint("S�g restaurant");
		} else if (beanObj.getGuideId().equalsIgnoreCase("60")) {
			search.setHint("S�g caf�");
		} else if (beanObj.getGuideId().equalsIgnoreCase("90")) {
			search.setHint("S�g ny�bnet");
		} else {
			search.setHint("S�g Bar");
		}
		sideBar = (SideBar) findViewById(R.id.sideBar);
		if (isnyanmeldt()) {
			a_z.setText(getString(R.string.senas));
			LinearLayout.LayoutParams a_zparam = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 0.9f);
			a_z.setLayoutParams(a_zparam);
			LinearLayout.LayoutParams citiesparam = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1.2f);
			cities.setLayoutParams(citiesparam);
			LinearLayout.LayoutParams catparam = new LinearLayout.LayoutParams(
					LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 0.9f);
			categories.setLayoutParams(catparam);
			sideBar.setVisibility(View.GONE);
		}
		if (null == background) {
			background = new Background();
		} else {
			background.cancel(true);
		}
		background.execute();
	}
	
	public boolean isnyanmeldt() {
		if (beanObj.getGuideName().equalsIgnoreCase(
				this.getString(R.string.nyanmeldt_name))) {
			return true;
		} else {
			return false;
		}
	}

	class Background extends AsyncTask<Void, Void, Void> {
		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			for (Restaurant temp : totalRestaurants) {
				if (temp.getRestaurantClass().contains("7")) {
					brastt_count++;
				} else if (temp.getRestaurantClass().contains("2")) {
					masterclas_count++;
				} else if (temp.getRestaurantClass().contains("4")) {
					godclass_count++;
				} else if (temp.getRestaurantClass().contains("3")) {
					myketclass_count++;
				} else if (temp.getRestaurantClass().contains("1")) {
					interclass_count++;
				}

			}
			brastt_count = interclass_count + myketclass_count + godclass_count
					+ masterclas_count;
			godclass_count = interclass_count + myketclass_count
					+ masterclas_count;
			myketclass_count = interclass_count + masterclas_count;
			masterclas_count = interclass_count;
			interclass_count = 0;
			azRestaurants.addAll(totalRestaurants);
			cityRestaurants.addAll(totalRestaurants);
			categoryRestaurants.addAll(totalRestaurants);

			for (Restaurant resta : totalRestaurants) {
				if (!citiesList.contains(resta.getCity())) {
					citiesList.add(resta.getCity());
				}
			}
			Collections.sort(citiesList, new Comparator<String>() {

				@Override
				public int compare(String p1, String p2) {
					return p1.compareToIgnoreCase(p2);
				}
			});
			
			if (isnyanmeldt()) {
				Collections.sort(azRestaurants, new Comparator<Restaurant>() {

					@Override
					public int compare(Restaurant p1, Restaurant p2) {
						return p2.getChanged().compareToIgnoreCase(p1.getChanged());
					}
				});

			} else {
			Collections.sort(azRestaurants, new Comparator<Restaurant>() {

				@Override
				public int compare(Restaurant p1, Restaurant p2) {
					return p1.getTitle().compareToIgnoreCase(p2.getTitle());
				}
			});
			}
			Collections.sort(cityRestaurants, new Comparator<Restaurant>() {

				@Override
				public int compare(Restaurant p1, Restaurant p2) {
					return p1.getTitle().compareToIgnoreCase(p2.getTitle());
				}
			});
			Collections.sort(categoryRestaurants, new Comparator<Restaurant>() {

				@Override
				public int compare(Restaurant p1, Restaurant p2) {
					return p1.getRestaurantClass().compareToIgnoreCase(
							p2.getRestaurantClass());
				}
			});
			adapter = new AZRestaurantsAdapter(
					RestaurantsList.this.getParent(), R.layout.layout_row,
					azRestaurants, beanObj.isGuideisBought()
							|| info.isThisGuidebought(beanObj.getGuideId(),
									beanObj.getGuideYear()), beanObj);
			catgAdapter = new CategoryRestaurantAdapter(RestaurantsList.this,
					R.layout.layout_row, categoryRestaurants);
			cityAdapter = new CityWiseRestaurantsAdapter(RestaurantsList.this,
					R.layout.layout_row, citiesList);
			list.setAdapter(adapter);
			sideBar.setListView(list);
			setSelections(true, false, false);
			setBackgrounds(Color.DKGRAY, Color.LTGRAY, Color.LTGRAY);
			setListeners();
			if (null != progress) {
				if (progress.isShowing())
					progress.dismiss();
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			String parse_url = null;
			if (dataHandler.readTreats().size() > 0) {
				totalRestaurants = dataHandler.readTreats();
			} else if (dataHandler.readTempData().size() > 0) {
				totalRestaurants = dataHandler.readTempData();
			} else {
				
				if (beanObj.getGuideName().equalsIgnoreCase(
						RestaurantsList.this.getString(R.string.nyanmeldt_name))) {
					parse_url = DataEngine.BASE_URL + DataEngine.nyanmeldt_INFO;
				} else {
					parse_url = DataEngine.BASE_URL
							+ DataEngine.ALL_RESTAURANTS_BASIC_INFO
							+ beanObj.getGuideId();
				}
				RestaurantsParser res_parser = new RestaurantsParser(
						RestaurantsList.this,parse_url , beanObj.getGuideName(),
						beanObj.getGuideId(), beanObj.getGuideYear());
				res_parser.doJsonparse();
				totalRestaurants = res_parser.getData();
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			if (null != progress) {
				if (!progress.isShowing())
					progress.show();
			}
			super.onPreExecute();
		}
	}

	/**
	 * Sets the slider view to be opened.
	 */
	public void calling(View v) {
		Home.setDrawer();
	}

	/**
	 * Does the initialization flags 
	 * @param azRes
	 * @param citylist
	 * @param cateList
	 */
	private void setSelections(boolean azRes, boolean citylist, boolean cateList) {
		azSelected = azRes;
		citiesSelected = citylist;
		categorySelected = cateList;
	}

	/**
	 * Represents all the listeners of the class
	 */
	public void setListeners() {
		findViewById(R.id.brasst).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				list.setSelection(brastt_count);
			}
		});
		findViewById(R.id.interclass).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				list.setSelection(interclass_count);
			}
		});
		findViewById(R.id.masterclass).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						list.setSelection(masterclas_count);
					}
				});
		findViewById(R.id.godclass).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				list.setSelection(godclass_count);
			}
		});
		findViewById(R.id.myketclass).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				list.setSelection(myketclass_count);
			}
		});
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Home.closeDrawer();
				if (azSelected) {
					Intent nextActivity = new Intent(RestaurantsList.this,
							DetailedRestaurantview.class);
					
					nextActivity.putExtra("RES", (Restaurant) arg0.getAdapter()
							.getItem(arg2));
					nextActivity.putExtra("GUIDE", beanObj);
					nextActivity.putExtra("PUSH",false);
					ListGroup parentActivity = (ListGroup) getParent();
					parentActivity.startActivity(nextActivity);
					/*parentActivity.startChildActivity("restaurantInformation",
							nextActivity);*/
				} else if (categorySelected) {
					Intent nextActivity = new Intent(RestaurantsList.this,
							DetailedRestaurantview.class);
					nextActivity.putExtra("RES", (Restaurant) arg0.getAdapter()
							.getItem(arg2));
					nextActivity.putExtra("GUIDE", beanObj);
					nextActivity.putExtra("PUSH",false);
					ListGroup parentActivity = (ListGroup) getParent();
					parentActivity.startActivity(nextActivity);
					/*parentActivity.startChildActivity("restaurantInformation",
							nextActivity);*/
				} else {
					Intent intent = new Intent(RestaurantsList.this,
							FilteredRestaurants.class);
					Bundle bundle = new Bundle();
					intent.putExtra("GUIDE", beanObj);
					bundle.putString("CITY", (String) arg0.getAdapter()
							.getItem(arg2));
					intent.putExtras(bundle);
					ListGroup parentActivity = (ListGroup) getParent();
					parentActivity.startChildActivity("cityinfo", intent);

				}
			}
		});
		search.addTextChangedListener(new TextWatcher() {

			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2,
					int arg3) {
				// When user changed the Text
				System.out.println("texxttt"+cs);
				if (azSelected)
				{
					RestaurantsList.this.adapter.getFilter().filter(cs);
				}
				if (citiesSelected){
					
					RestaurantsList.this.cityAdapter.getFilter().filter(cs);
				}
				if (categorySelected){
					
					RestaurantsList.this.catgAdapter.getFilter().filter(cs);
				}
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {
			}
		});
		a_z.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (beanObj.getGuideId().matches("58")) {
					search.setHint(R.string.restaurant_search);
				} else if (beanObj.getGuideId().matches("60")) {
					search.setHint(R.string.cafe_search);
				} else if (beanObj.getGuideId().matches("90")) {
					search.setHint(R.string.nyanmeldt_search);
				} else {
					search.setHint(R.string.bar_search);
				}
				list.setVisibility(View.VISIBLE);

				list.setAdapter(adapter);
				sideBar.setListView(list);
				adapter.notifyDataSetChanged();
				setSelections(true, false, false);
				setBackgrounds(Color.DKGRAY, Color.LTGRAY, Color.LTGRAY);
				if(!iskeyboardvisible)

				sideBar.setVisibility(View.VISIBLE);
				findViewById(R.id.classifier).setVisibility(View.GONE);
			}
		});
		cities.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				search.setHint("S�g by");
				list.setVisibility(View.VISIBLE);

				list.setAdapter(cityAdapter);
				
				sideBar.setListView(list);
				cityAdapter.notifyDataSetChanged();
				setSelections(false, true, false);
				setBackgrounds(Color.LTGRAY, Color.DKGRAY, Color.LTGRAY);
				if(!iskeyboardvisible)

				sideBar.setVisibility(View.VISIBLE);
				findViewById(R.id.classifier).setVisibility(View.GONE);

			}
		});
		categories.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (beanObj.getGuideId().matches("58")) {
					search.setHint(R.string.restaurant_search);
				} else if (beanObj.getGuideId().matches("60")) {
					search.setHint(R.string.cafe_search);
				} else if (beanObj.getGuideId().matches("90")) {
					search.setHint(R.string.nyanmeldt_search);
				} else {
					search.setHint(R.string.bar_search);
				}
				list.setAdapter(catgAdapter);
				sideBar.setListView(list);
				catgAdapter.notifyDataSetChanged();
				setSelections(false, false, true);
				setBackgrounds(Color.LTGRAY, Color.LTGRAY, Color.DKGRAY);
				sideBar.setVisibility(View.GONE);
				if (isnyanmeldt() || beanObj.isGuideisBought()
						|| info.isThisGuidebought(beanObj.getGuideId(),
								beanObj.getGuideYear())) {
					findViewById(R.id.classifier).setVisibility(View.VISIBLE);
					list.setVisibility(View.VISIBLE);
				} else {
					list.setVisibility(View.GONE);
					showAlertBox();
				}
			}
		});
	}

	/**
	 *   Alerts to buy the guide if not brought.
	 */
	protected void showAlertBox() {
		// TODO Auto-generated method stub
		AlertDialog.Builder builder = new AlertDialog.Builder(this.getParent());
		builder.setMessage(getString(R.string.guide_buytxt_bef)+" "+beanObj.getGuidePrice()+" "+getString(R.string.guidetxt_after));
		builder.setPositiveButton(getString(R.string.ok),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						Home.setTab(4);
					}
				});
		builder.setNegativeButton(getString(R.string.cancel),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						dialog.cancel();
					}
				});
		builder.show();

	}

	/**
	 * sets the background colors dynamically on selection of buttons
	 * @param azBackgroud
	 * @param citiesBackgroudn
	 * @param categriesBackground
	 */
	protected void setBackgrounds(int azBackgroud, int citiesBackgroudn,
			int categriesBackground) {
		// TODO Auto-generated method stub
		a_z.setBackgroundColor(azBackgroud);
		cities.setBackgroundColor(citiesBackgroudn);
		categories.setBackgroundColor(categriesBackground);
	}

	/**
	 * Restaurant Adapter class
	 */
	class MyAdapter extends BaseAdapter {
		Vector<Restaurant> restaurantsList;
		LayoutInflater inflater;
		Button selection;

		public MyAdapter(Context context, Vector<Restaurant> list,
				Button selector) {
			restaurantsList = new Vector<Restaurant>();
			restaurantsList.addAll(list);
			inflater = LayoutInflater.from(context);
			selection = selector;
		}

		@Override
		public int getCount() {
			return restaurantsList.size();
		}

		@Override
		public Object getItem(int arg0) {
			return arg0;
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View convertView, ViewGroup arg2) {
			return convertView;
		}

	}
}
