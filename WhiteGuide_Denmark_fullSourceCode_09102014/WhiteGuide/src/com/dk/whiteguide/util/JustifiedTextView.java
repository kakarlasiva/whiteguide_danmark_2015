package com.dk.whiteguide.util;

import java.util.Vector;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.webkit.WebChromeClient;
import android.webkit.WebView;

import com.dk.whiteguide.helpers.BasicInfoBean;

public class JustifiedTextView extends WebView{
	private String guide_core      = "<html><body style='text-align:justify;color:rgba(%s);font-size:%dpx;margin: 10px 10px 30px 10px;'>%s</body></html>";
    private String core      = "<html><body style='text-align:justify;color:rgba(%s);font-size:%dpx;margin: 10px 10px 10px 10px;'>%s</body></html>";
    private String textColor = "0,0,0,255";
    private String text      = "Her kan du k�be adgang til anmeldelserne i databasen. N�r White Guide 2015 udkommer, kan du k�be adgang til de nye anmeldelser uden at skulle downloade en ny app.";
    private int textSize     = 16;
    private int backgroundColor=Color.WHITE;
    Vector<BasicInfoBean> bean_obj;
    private boolean isguide;

    public JustifiedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setWebChromeClient(new WebChromeClient(){});
    }

    public void setText(String s,boolean guidebrought){
    	this.isguide=guidebrought;
        this.text = s;
        reloadData();
    }

    @SuppressLint("NewApi")
    private void reloadData(){
    	
    	this.setVerticalScrollBarEnabled(true);
        // loadData(...) has a bug showing utf-8 correctly. That's why we need to set it first.
        this.getSettings().setDefaultTextEncodingName("utf-8");
        //String header = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
    	this.getSettings().setJavaScriptEnabled(true); 
    	
    	if(isguide){
    		this.loadData(String.format(guide_core,textColor,textSize,text), "text/html; charset=UTF-8", null);
    	}else{
        this.loadData(String.format(core,textColor,textSize,text), "text/html; charset=UTF-8", null);
    	}
       
        //this.loadData(String.format(core,textColor,textSize,text), "text/html","charset=UTF-8");

        // set WebView's background color *after* data was loaded.
        super.setBackgroundColor(backgroundColor);

        // Hardware rendering breaks background color to work as expected.
        // Need to use software renderer in that case.
        //if(android.os.Build.VERSION.SDK_INT >= 11)
           //this.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
    }
    
    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	// TODO Auto-generated method stub
    	//requestDisallowInterceptTouchEvent(true);
    	return super.onTouchEvent(event);
    }

    public void setTextColor(int hex){
        String h = Integer.toHexString(hex);
        int a = Integer.parseInt(h.substring(0, 2),16);
        int r = Integer.parseInt(h.substring(2, 4),16);
        int g = Integer.parseInt(h.substring(4, 6),16);
        int b = Integer.parseInt(h.substring(6, 8),16);
        textColor = String.format("%d,%d,%d,%d", r, g, b, a); 
        reloadData();
    }

    public void setBackgroundColor(int hex){
        backgroundColor = hex;
        reloadData();
    }

    public void setTextSize(int textSize){
        this.textSize = textSize;
        reloadData();
    }
   

	public void setMaxLines(int i) {
		// TODO Auto-generated method stub
		text.substring(0, i);
	}
}
