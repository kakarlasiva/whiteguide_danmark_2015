package com.dk.whiteguide.dataengine;

public class DataEngine {
	public static final String BASE_URL = "http://www.whiteguide.dk/apidk/";
	// new Dev site "http://wgdenmark.solidpartner.com/apidk/"
	// old DEV SITE "http://whiteguide.solidpartner.com/api/";
	// Live site "http://www.whiteguide.dk/apidk/"

	public static final String nyanmeldt_INFO="getnytestatrestaurant.json";
	public static final String NEWS_URL_LINK = "getnewsfeed.json";
	public static final String NEWS_URL_PAGE = "getnewsfeed.json?page=";
	public static final String DETAILED_PULLURL="getnewsfeeddetail.json?nid=";

	public static final String ALL_RESTAURANTS_BASIC_INFO = "restaurants.json?tid=";
	public static final String APP_USERS = "appusers";
	public static final String FB_APPID = "530323050349767";
	public static final String GCM_URL = "gcmpushnotify";
	public static boolean showAds = true;
	public static String consumer_key = "gFUtrooktPrH1nkrvIzGQ";
	public static String secret_key = "g36tP2wnGyDvcCqtI0AF2svwk7tYMd3tCf5LZdKOOw";
	
	public static final String ERROR_LOG = "error";
    public static final String VERBOSE_LOG = "verbose";
    public static final String INFO_LOG = "info";
	public static final String DEBUG_LOG = "debug";

	// The log enabled/disabled status.
	public static final boolean ERROR_LOG_ENABLED = true;
	public static final boolean VERBOSE_LOG_ENABLED = true;
	public static final boolean INFO_LOG_ENABLED = true;
	public static final boolean DEBUG_LOG_ENABLED = true;

	public static String ERROR_MSG = "error_issue";

	public static final String SETTINGS_NAME = "settings";
	public static final String SETTINGS_MAIL = "mail";
	public static final String SETTINGS_NYTEST="Nytest_not";
	public static final String SETTINGS_NHYTER="Nhyter_not";
	public static final String SETTINGS_GCM="reg_id";
	public static final String SETTINGS_MAPRESUME = "mapresume";
	public static final String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAgN1PBW+mbsYxaXHA4O3FPlRmyPKeVSJKp2XrNaXwHXLvoE4rExz6gY7VuTREYzduDLPI4JesyAtBUeb5IOvE/pzIqkLPky7pp3bvQxPlS7JgH7rTynwIOcmtWqJKeZ6CYj4cYZiGfBbpOSB6IDZniBwmO5pDAQWKKYuw0t074/avu8Hvl6dXCIfw6kuqPLtnekxF39yiC4TB2a1BE0nV16G9nz7p4d0JVVGgBuiBQPXKOiPPvJ6SfRj/hQdTWRV5KbGbNCpArUSOntqTqQnAoEZIWOZdqtG0TyqPCXLwXyV+NurswGvgblI1imNeCWar22n97YI7nDMpfvL7Z8YkxwIDAQAB";

	
	public static boolean guidePurchased=false;
	// This is the new key with suresh keystore:
	// AIzaSyDhNsv_FjzGrpZKL41rL7u20ReMAhsGmYU
	// AIzaSyAZSfU6DKvDh8JuQ0KvRDAuNdngKDWbn9o
	// for whiteguide 25:1A:1B:6C:E3:51:F0:30:47:85:FD:E0:37:C0:72:D7

}
