package com.dk.whiteguide.dataengine;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

import android.content.Context;

import com.dk.whiteguide.helpers.BasicInfoBean;

public class DataHandler {
	DataHandler treatHandler;
	Context cntxt;
	private String dataFile;
	private String tempFile;

	public DataHandler(Context context) {
		this.cntxt = context;
	}

	@SuppressWarnings("unchecked")
	public int getDataCount(Vector<BasicInfoBean> beanList) {
		int dataCount = 0;
		for (BasicInfoBean basicInfoBean : beanList) {
			dataFile = basicInfoBean.getGuideName() + ".txt";
			try {
				FileInputStream fis = cntxt.openFileInput(dataFile);
				ObjectInputStream is = new ObjectInputStream(fis);
				dataCount += ((Vector<Restaurant>) is.readObject()).size();
				is.close();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		return dataCount;
	}
	
	@SuppressWarnings("unchecked")
	public int getDataCount(BasicInfoBean basicInfoBean) {
		int dataCount = 0;
		
			dataFile = basicInfoBean.getGuideName() + ".txt";
			try {
				FileInputStream fis = cntxt.openFileInput(dataFile);
				ObjectInputStream is = new ObjectInputStream(fis);
				dataCount += ((Vector<Restaurant>) is.readObject()).size();
				is.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return dataCount;
	}

	public DataHandler(Context context, String fielName) {
		cntxt = context;
		dataFile = fielName + ".txt";
		tempFile = fielName + "_temp.txt";
	}

	@SuppressWarnings("unchecked")
	public Vector<Restaurant> readTreats() {
		Vector<Restaurant> dummyRestaurants = null;
		try {
			FileInputStream fis = cntxt.openFileInput(dataFile);
			ObjectInputStream is = new ObjectInputStream(fis);
			dummyRestaurants = new Vector<Restaurant>();
			dummyRestaurants.addAll((Vector<Restaurant>) is.readObject());
			is.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (null == dummyRestaurants) {
			dummyRestaurants = new Vector<Restaurant>();
		}
		return dummyRestaurants;
	}

	public void updateTreat(String treatId, Restaurant treat) {
		Vector<Restaurant> dummyRestaurants = null;
		dummyRestaurants = readTreats();
		Restaurant treatToupdate = null;
		for (Restaurant temp : dummyRestaurants) {
			if (temp.getId().equalsIgnoreCase(treatId)) {
				treatToupdate = temp;
				break;
			}
		}
		dummyRestaurants.remove(treatToupdate);
		dummyRestaurants.add(treat);
		try {
			FileOutputStream fos = cntxt.openFileOutput(tempFile,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dummyRestaurants);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void deleteTreat(String treatIdtoDel) {
		Vector<Restaurant> dummyRestaurants = null;
		dummyRestaurants = readTreats();
		Restaurant treatToDel = null;
		for (Restaurant temp : dummyRestaurants) {
			if (temp.getId().equalsIgnoreCase(treatIdtoDel)) {
				treatToDel = temp;
				break;
			}
		}
		dummyRestaurants.remove(treatToDel);
		try {
			FileOutputStream fos = cntxt.openFileOutput(dataFile,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dummyRestaurants);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void writeAll(Vector<Restaurant> restList) {
		Vector<Restaurant> dummyRestaurants = new Vector<Restaurant>();
		dummyRestaurants = restList;
		try {
			FileOutputStream fos = cntxt.openFileOutput(dataFile,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dummyRestaurants);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public Vector<Restaurant> readTempData() {
		Vector<Restaurant> dummyRestaurants = null;
		try {
			FileInputStream fis = cntxt.openFileInput(tempFile);
			ObjectInputStream is = new ObjectInputStream(fis);
			dummyRestaurants = new Vector<Restaurant>();
			dummyRestaurants.addAll((Vector<Restaurant>) is.readObject());
			is.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

		if (null == dummyRestaurants) {
			dummyRestaurants = new Vector<Restaurant>();
		}
		return dummyRestaurants;
	}

	public void writetoTemp(Vector<Restaurant> restList) {
		// TODO Auto-generated method stub
		Vector<Restaurant> dummyRestaurants = new Vector<Restaurant>();
		dummyRestaurants = restList;
		try {
			FileOutputStream fos = cntxt.openFileOutput(tempFile,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dummyRestaurants);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	public void updateAll(Vector<Restaurant> restList) {
		// TODO Auto-generated method stub
		Vector<Restaurant> dummyRestaurants = new Vector<Restaurant>();
		dummyRestaurants = restList;
		try {
			FileOutputStream fos = cntxt.openFileOutput(dataFile,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dummyRestaurants);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
