package com.dk.whiteguide.dataengine;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;

import android.content.Context;

public class FavouritesHandler {
	FavouritesHandler treatHandler;
	Vector<Restaurant> restaurantList;
	Context cntxt;
	private String fileName = "Favourites.txt";

	public FavouritesHandler(Context context) {
		cntxt = context;
	}

	@SuppressWarnings("unchecked")
	public Vector<Restaurant> readTreats() {
		try {
			FileInputStream fis = cntxt.openFileInput(fileName);
			ObjectInputStream is = new ObjectInputStream(fis);
			restaurantList = new Vector<Restaurant>();
			restaurantList.addAll((Vector<Restaurant>) is.readObject());
			is.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

		if (null == restaurantList) {
			restaurantList = new Vector<Restaurant>();
		}
		return restaurantList;
	}

	public void updateTreat(String treatId, Restaurant treat) {
		restaurantList = readTreats();
		Restaurant treatToupdate = null;
		for (Restaurant temp : restaurantList) {
			if (temp.getId().equalsIgnoreCase(treatId)) {
				treatToupdate = temp;
				break;
			}
		}
		restaurantList.remove(treatToupdate);
		restaurantList.add(treat);
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(restaurantList);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void deleteTreat(String treatIdtoDel) {
		restaurantList = readTreats();
		Restaurant treatToDel = null;
		for (Restaurant temp : restaurantList) {
			if (temp.getId().equalsIgnoreCase(treatIdtoDel)) {
				treatToDel = temp;
				break;
			}
		}
		restaurantList.remove(treatToDel);
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(restaurantList);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void writeTreat(Restaurant treat) {
		restaurantList = readTreats();
		boolean isThere = false;
		boolean isChanged = false;
		for (Restaurant rest : restaurantList) {
			if (treat.getId().equalsIgnoreCase(rest.getId())) {
				isThere = true;
				if (!treat.getChanged().equalsIgnoreCase(rest.getChanged())) {
					isChanged = true;
				}
				break;
			}
		}
		if (!isThere)
			restaurantList.add(treat);
		else if (isChanged)
			updateTreat(treat.getId(), treat);
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(restaurantList);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public void deleteAll() {
		// TODO Auto-generated method stub
		restaurantList = readTreats();
		restaurantList.clear();
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(restaurantList);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
}
