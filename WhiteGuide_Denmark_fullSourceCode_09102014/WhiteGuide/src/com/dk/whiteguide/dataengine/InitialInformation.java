package com.dk.whiteguide.dataengine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.Vector;

import android.content.Context;

import com.dk.whiteguide.helpers.BasicInfoBean;

public class InitialInformation {
	Vector<BasicInfoBean> beanList;
	Context cntxt;
	private String fileName = "Information.txt";
	public DataChanged dataChanged;
	public String requestFor;

	public InitialInformation(Context contxt) {
		this.cntxt = contxt;
	}

	public boolean isAtleastOneguideBought() {
		boolean isbought = false;
		beanList = readData();
		for (BasicInfoBean temp : beanList) {
			if (temp.isGuideisBought())
				return true;
		}
		return isbought;
	}
	
	public boolean isnyanmeldt_shown(){
		boolean isnyanmeldt = false;
		beanList = readData();
		for (BasicInfoBean temp : beanList) {
			if (temp.isNyanmeldt_enabled())
				return true;
		}
		return isnyanmeldt;
	}

	public void setListenerforthis(String id, DataChanged changedListener) {
		dataChanged = changedListener;
		requestFor = id;
	}

	public void updateTreat(String treatId, BasicInfoBean treat) {
		beanList = readData();
		BasicInfoBean treatToupdate = null;
		/*for (BasicInfoBean temp : beanList) {
			if (temp.getGuideId().equalsIgnoreCase(treatId)) {
				treatToupdate = temp;
				break;
			}
		}
		beanList.remove(treatToupdate);
		beanList.add(treat);*/
		int position = 0;
		  for (BasicInfoBean temp : beanList) {
		   if (temp.getGuideId().equalsIgnoreCase(treatId)) {
		    treatToupdate = temp;
		    position=beanList.indexOf(treatToupdate);
		    break;
		   }
		  }
		  beanList.set(position, treat);
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(beanList);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		try {
			if (null != dataChanged && treatId.equalsIgnoreCase(requestFor)) {
				dataChanged.dataChanged(treat);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public Vector<BasicInfoBean> readData() {
		try {
			FileInputStream fis = cntxt.openFileInput(fileName);
			ObjectInputStream is = new ObjectInputStream(fis);
			beanList = new Vector<BasicInfoBean>();
			beanList = (Vector<BasicInfoBean>) is.readObject();
			is.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (Exception e) {
			// TODO: handle exception
		}

		if (null == beanList) {
			beanList = new Vector<BasicInfoBean>();
		}

		return beanList;
	}

	public void writeAll(Vector<BasicInfoBean> _beanList) {
		if (null == beanList)
			beanList = new Vector<BasicInfoBean>();
		beanList = _beanList;
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(beanList);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public interface DataChanged {
		public void dataChanged(BasicInfoBean beanObj);
	}
}
