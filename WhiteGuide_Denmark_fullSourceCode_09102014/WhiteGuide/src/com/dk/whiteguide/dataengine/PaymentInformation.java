package com.dk.whiteguide.dataengine;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.Vector;

import android.content.Context;
import android.util.Log;

import com.dk.whiteguide.helpers.BasicInfoBean;

public class PaymentInformation {
	Vector<BasicInfoBean> beanList;
	Context cntxt;
	private String fileName = "Payments.txt";
	public String requestFor;

	public PaymentInformation(Context contxt) {
		this.cntxt = contxt;
	}

	public boolean isAtleastOneguideBought() {
		boolean isbought = false;
		beanList = readData();
		for (BasicInfoBean temp : beanList) {
			if (temp.isGuideisBought())
				return true;
		}
		return isbought;
	}
	
	public void clearAll() {
		beanList = new Vector<BasicInfoBean>();
	try {
		FileOutputStream fos = cntxt.openFileOutput(fileName,
				Context.MODE_PRIVATE);
		ObjectOutputStream os = new ObjectOutputStream(fos);
		os.writeObject(beanList);
		os.close();
	} catch (Exception e) {
		// TODO: handle exception
		e.printStackTrace();
	}
}

	public void writeTreat(BasicInfoBean treat) {
		beanList = readData();
		beanList.add(treat);
		try {
			FileOutputStream fos = cntxt.openFileOutput(fileName,
					Context.MODE_PRIVATE);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(beanList);
			os.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public boolean isThisGuidebought(String guideId, String guideYear) {
		boolean isbought = false;
		beanList = readData();
		for (BasicInfoBean temp : beanList) {
			if (temp.getGuideId().equalsIgnoreCase(guideId)
					) {
				isbought = true;
				break;
			}
		}

		return isbought;
	}

	@SuppressWarnings("unchecked")
	public Vector<BasicInfoBean> readData() {
		try {
			FileInputStream fis = cntxt.openFileInput(fileName);
			ObjectInputStream is = new ObjectInputStream(fis);
			beanList = new Vector<BasicInfoBean>();
			beanList = (Vector<BasicInfoBean>) is.readObject();
			is.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i("BASIC", "Read File: " + e.getMessage());

		} catch (StreamCorruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i("BASIC", "Read File: " + e.getMessage());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i("BASIC", "Read File: " + e.getMessage());

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Log.i("BASIC", "Read File: " + e.getMessage());

		} catch (Exception e) {
			// TODO: handle exception
		}

		if (null == beanList) {
			beanList = new Vector<BasicInfoBean>();
		}

		return beanList;
	}
}
