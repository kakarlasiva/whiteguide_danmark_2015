package com.dk.whiteguide.helpers;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.LinearLayout;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.dk.whiteguide.dataengine.Restaurant;
import com.live.dk.whiteguide.R;

public class CategoryRestaurantAdapter extends ArrayAdapter<Restaurant>
		implements SectionIndexer {

	private List<Restaurant> stringArray;
	private Context context;
	protected List<Restaurant> list;
	protected ArrayList<Restaurant> sortedList;
	LayoutInflater inflate;
	//Typeface t;

	public CategoryRestaurantAdapter(Context cntxt, int resource,
			List<Restaurant> data) {
		// TODO Auto-generated constructor stub
		super(cntxt, resource, data);
		stringArray = data;
		this.context = cntxt;
		list = data;
		//t = Typeface.createFromAsset(getContext().getAssets(),
				//"HelveticaNeue.ttf");
		inflate = ((Activity) context).getLayoutInflater();
	}

	public int getCount() {
		return stringArray.size();
	}

	public Restaurant getItem(int arg0) {
		return stringArray.get(arg0);
	}

	public long getItemId(int arg0) {
		return 0;
	}
	String label="",preLabel="";
	char firstChar,preFirstChar;
	public View getView(int position, View v, ViewGroup parent) {
		View view = (View) inflate.inflate(R.layout.layout_row, null);
		LinearLayout header = (LinearLayout) view.findViewById(R.id.section);
		try{
		label = stringArray.get(position).getRestaurantClass();
		firstChar = label.toUpperCase().charAt(0);
		}catch(Exception e){
			label="#";
			firstChar='#';
		}
		if (TextUtils.isDigitsOnly(label.substring(0, 1))) {
			label = label.replace(label.substring(0, 1), "");
			label = label.replace(".", "");
			label = label.trim();
		}
		if (position == 0) {
			setSection(header, label);
		} else {
			try{
			 preLabel = stringArray.get(position - 1)
					.getRestaurantClass();
			 preFirstChar = preLabel.toUpperCase().charAt(0);

			}catch(Exception es){
				preLabel="#";
				preFirstChar='#';
			}
			if (firstChar != preFirstChar) {
				setSection(header, label);
			} else {
				header.setVisibility(View.GONE);
			}
		}
		label = stringArray.get(position).getTitle();
		TextView textView = (TextView) view.findViewById(R.id.restaurant_name);
		try {
			if (label.startsWith("#")) {
				label = label.substring(1, label.length());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		textView.setText(label);
		//textView.setTypeface(t);
		TextView textView2 = (TextView) view.findViewById(R.id.city_name);
		textView2.setText(stringArray.get(position).getCity());
		//textView2.setTypeface(t);
		return view;
	}

	private void setSection(LinearLayout header, String label) {
		TextView text = new TextView(context);
		header.setBackgroundColor(Color.BLACK);
		text.setTextColor(Color.WHITE);
		text.setText(label);
		text.setTextSize(13);
		text.setPadding(5, 0, 0, 0);
		text.setGravity(Gravity.CENTER_VERTICAL);
		//text.setTypeface(t);
		header.addView(text);
	}

	public int getPositionForSection(int section) {
		if (section == 35) {
			return 0;
		}
		for (int i = 0; i < stringArray.size(); i++) {
			String l = stringArray.get(i).getTitle();
			char firstChar = l.toUpperCase().charAt(0);
			if (firstChar == section) {
				return i;
			}
		}
		return -1;
	}

	public int getSectionForPosition(int arg0) {
		return 0;
	}

	public Object[] getSections() {
		return null;
	}

	@Override
	public Filter getFilter() {
		Filter filter = new Filter() {

			@SuppressWarnings("unchecked")
			@Override
			protected void publishResults(CharSequence constraint,
					FilterResults results) {
				stringArray = (List<Restaurant>) results.values;
				notifyDataSetChanged();
			}

			@Override
			protected FilterResults performFiltering(CharSequence constraint) {
				FilterResults results = new FilterResults();
				List<Restaurant> FilteredArrList = new ArrayList<Restaurant>();
				sortedList = new ArrayList<Restaurant>(list);

				if (constraint == null || constraint.length() == 0) {
					// set the Original result to return
					results.count = sortedList.size();
					results.values = sortedList;
				} else {
					constraint = constraint.toString().toLowerCase();
					for (int i = 0; i < sortedList.size(); i++) {
						Restaurant data = sortedList.get(i);
						if ((data.getTitle().toLowerCase()
								.contains(constraint.toString())||(data.getTitle().toLowerCase()
										.contains(constraint.toString().replace("�", "aa").replace("�", "aa"))) || (data.getTitle().toLowerCase()
												.contains(constraint.toString().replace("aa", "�").replace("Aa", "�"))))) {
							FilteredArrList.add(data);
						}
					}
					// set the Filtered result to return
					results.count = FilteredArrList.size();
					results.values = FilteredArrList;
				}
				return results;
			}
		};
		return filter;
	}

}