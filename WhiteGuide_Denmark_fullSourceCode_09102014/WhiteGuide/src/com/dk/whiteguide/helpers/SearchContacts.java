package com.dk.whiteguide.helpers;

import java.util.List;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.MultiAutoCompleteTextView;

import com.dk.whiteguide.dataengine.Restaurant;

public class SearchContacts extends MultiAutoCompleteTextView implements
		OnItemClickListener {
	ContactSelected selected;

	public SearchContacts(Context context) {
		super(context);
	}

	/* Constructor */
	public SearchContacts(Context context,
			List<Restaurant> contacts) {
		super(context);
	}

	/* Constructor */
	public SearchContacts(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	/* Constructor */
	public SearchContacts(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
	}


	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		// TODO Auto-generated method stub
		selected.contactSelected(arg0.getItemAtPosition(arg2));
	}

	public void setItemClickListener(ContactSelected _selected) {
		this.selected = _selected;
	}

	public interface ContactSelected {
		public void contactSelected(Object contact);
	}
}
