package com.dk.whiteguide.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.telephony.TelephonyManager;

import com.dk.whiteguide.dataengine.DataEngine;

/**
 * This class uses the AccountManager to get the primary email address of the
 * current user.
 */

public class UserEmailFetcher {

	static SharedPreferences settings;
	@SuppressWarnings("static-access")
	public static String getEmail(Context context) {
		/*AccountManager accountManager = AccountManager.get(context);
		Account account = getAccount(accountManager);
		if (account == null) {
			return null;
		} else {
			return account.name;
		}*/
		settings = context.getSharedPreferences(DataEngine.SETTINGS_NAME, context.MODE_PRIVATE);
		return settings.getString(DataEngine.SETTINGS_MAIL, null);

		
	}

	public static String getDeviceId(Context cntxt) {
		// TODO Auto-generated method stub
		String devId = null;
		devId = ((TelephonyManager) cntxt
				.getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
		if (null == devId) {
			devId = android.provider.Settings.Secure.getString(
					cntxt.getContentResolver(),
					android.provider.Settings.Secure.ANDROID_ID);
		}
		
		return devId;
	}

	
}